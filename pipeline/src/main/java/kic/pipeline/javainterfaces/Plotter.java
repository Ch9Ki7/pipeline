package kic.pipeline.javainterfaces;

import java.util.Map;

/**
 * Created by kindler on 20/01/2017.
 */
public interface Plotter {
    void onChart(String chartId);

    void onDataSet(String dataSetId, String type);

    void plot(String chartId, String dataSetId, Object x, Object y);

    void plot(String chartId, String dataSetId, Object x, Object y, Map subChart);
}
