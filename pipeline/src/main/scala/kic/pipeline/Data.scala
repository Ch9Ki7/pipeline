package kic.pipeline


import java.util.Calendar

import akka.stream._
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source}
import akka.{Done, NotUsed}
import kic.pipeline.`lazy`.Logging
import kic.pipeline.flows.{Quant, Synchronizer}
import kic.pipeline.javainterfaces.Plotter
import kic.pipeline.stages.Weights

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future

/**
  * Created by kic on 13.11.16.
  *
  * Process check latest date on status table
  * val q3 = for {
      s <- status if item === $item && indicator = $indicator
    } yield (c.name, s.name)

  * if null or to less then fetch and store to database
  * fetch and stream from database
  *
  * DOCS:
  *   working with graphs: http://doc.akka.io/docs/akka/2.4.2/scala/stream/stream-graphs.html
  *   Stream composition: http://doc.akka.io/docs/akka/2.4/scala/stream/stream-composition.html
  *
  */
object Data extends Logging {

  def yahooData(symbol: String, indicator: String, fromDate: ISODate, toDate: ISODate): Source[(ISODate, Double), NotUsed] = {
    val indicatorIdx = Map[String, Int]("OPEN" -> 1, "HIGH" -> 2, "LOW" -> 3, "CLOSE" -> 4, "VOLUME" -> 5, "ADJ-CLOSE" -> 6)
    val frmMonth_1 = fromDate.month - 1
    val frmDay = fromDate.day
    val frmYear = fromDate.year
    val toMonth_1 = toDate.month - 1
    val toDay = toDate.day
    val toYear = toDate.year

    // http://chart.finance.yahoo.com/table.csv?s=AA&a=0&b=2&c=1970&d=10&e=8&f=2016&g=d&ignore=.csv
    val path = "/table.csv"
    val queryString = s"s=$symbol&a=$frmMonth_1&b=$frmDay&c=$frmYear&d=$toMonth_1&e=$toDay&f=$toYear&g=d&ignore=.csv"
    val url = "http://chart.finance.yahoo.com" + path + "?" + queryString

    debug(url)
    val buffer = new ListBuffer[(ISODate, Double)]
    val idx: Int = indicatorIdx.get(indicator.toUpperCase).get
    val name = symbol+ "::" + indicator

    Source.fromIterator(() => scala.io.Source.fromURL(url)
        .getLines()
        .drop(1)
        .map(line => line.split(","))
        //.filter(cols => cols(idx).isDoubleNumber())
        .map(cols => (cols(0).toISODate(), cols(idx).toDouble))
        .toList
        .reverseIterator
    ).named(name)
  }



  /* --------------------------------------------------------------------------------------------------------------  */


  def plotPricesGraph(plotter: Plotter, benchmarkSymbol: String, symbols: Array[String]): Unit = {
    import kic.pipeline.implicits.Conversions.tupleOrdering
    val startDate = new ISODate("2015-01-01")
    val endDate =  new ISODate("2016-01-31")
    val covarianceWindowSize = 100 // this will be a parameter later
    val qt = Quant[ISODate, Double]

    type E = (ISODate, Double)
    val plot = qt.plotSink[Any]("main")(plotter)

    // next we want to make a PlotSink
    val g = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._
      var shelfRange = (0 until symbols.length)

      val sources = symbols.map(symbol => b.add(Data.yahooData(symbol, "close", startDate, endDate)))
      val sync = b.add(Synchronizer[E]("plotPricesGraph", symbols.length))
      val price2ret = shelfRange.map(i => b.add(qt.price2Return()()))
      val ret2perf = shelfRange.map(i => b.add(qt.return2Performance()()))
      val plotLine = (s: String) => b.add(plot(s, "line"))

      shelfRange.foreach(i => {sources(i) ~> sync.inlets(i); sync.outlets(i) ~> price2ret(i) ~> ret2perf(i) ~> plotLine(symbols(i))})

      ClosedShape
    })

    g.run()
  }



  def optimizeGraph(plotter: Plotter, benchmarkSymbol: String, symbols: Array[String]): Unit = {
    val startDate = new ISODate("2015-01-01")
    val endDate =  new ISODate("2016-01-31")
    val covarianceWindowSize = 100 // this will be a parameter later
    val qt = Quant[ISODate, Double]
    val plot = qt.plotSink[Double]("main")(plotter)
    val plotFlow = qt.plotFlow[Double]("main")(plotter)
    val plotExtra = qt.plotSinkExtra("main")(plotter)

    type E = (ISODate, Double)

    val g = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val cov = b.add(qt.fanIntoCovariance(symbols.length, 6))
      val obsRet = b.add(qt.fanIntoRowVector(symbols.length))
      val expRet = b.add(qt.fanIntoRowVector(symbols.length))
      val bench = b.add(yahooData(benchmarkSymbol, "close", startDate, endDate))
      val benchToRet = b.add(qt.price2Return()())
      val opt = b.add(qt.optimizePortfolio(symbols.length, 50))
      val r2p = () => b.add(qt.return2Performance()())
      val plotLine = (s: String) => b.add(plot(s, "line"))
      val plotLineFlow = (s: String) => b.add(plotFlow(s, "line"))
      val plotPortfolio = plotLineFlow("Portfolio")
      val plotReBalance = b.add(plotExtra("Re-balance", "line"))
      val extraData = b.add(Flow[(ISODate, Weights)].map({case (k, w) => if (w.rebalanced) w.toMap() else Map.empty[String, Double]}))

      symbols.indices.map(i => {
        val src = b.add(yahooData(symbols(i), "close", startDate, endDate))
        val toRet = b.add(qt.price2Return()())
        val bcRet = b.add(Broadcast[E](3))
        val toPerf = b.add(qt.return2Performance()())
        val macd = b.add(qt.macd(3, 5, 0, qt.sma)())

        src ~> toRet ~> bcRet.in
                        bcRet.out(0) ~> cov.in(i)
                        bcRet.out(1) ~> obsRet.in(i)
                        bcRet.out(2) ~> toPerf ~> macd.in
                                                  macd.outlets(0) ~> expRet.in(i)
                                                  macd.outlets(1) ~> Sink.ignore
                                                  macd.outlets(2) ~> Sink.ignore
      })

      expRet.out               ~> opt.in0ExpectedReturns
      obsRet.out               ~> opt.in1ObservedReturns
      cov.out                  ~> opt.in2Covariances
      bench      ~> benchToRet ~> opt.in3BenchmarkReturns
                                  opt.out0Weights           ~> extraData ~> plotReBalance.in1
                                  opt.out1Rebalanced        ~> Sink.ignore
                                  opt.out2PortfolioRisk     ~> Sink.ignore
                                  opt.out3PortfolioReturn   ~> r2p() ~> plotPortfolio ~> plotReBalance.in0
                                  opt.out4BenchmarkReturn   ~> r2p() ~> plotLine("Benchmark")
                                  opt.out5Alpha             ~> Sink.ignore
                                  opt.out6OneOverNRisk      ~> Sink.ignore
                                  opt.out7OneOverNReturn    ~> r2p() ~> plotLine("1/N")

      ClosedShape
    })

    g.run()
  }


  class SimpeDate(year: Int, month: Int, day: Int)
  case class FromDate(year: Int=1970, month: Int=1, day: Int=1) extends SimpeDate(year, month, day)
  case class ToDate(year: Int=Calendar.getInstance().get(Calendar.YEAR), month: Int=Calendar.getInstance().get(Calendar.MONTH), day: Int=Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) extends SimpeDate(year, month, day)

  def getQuotes(items: List[String], fromDate: SimpeDate, toDate: SimpeDate):List[Source[(ISODate, Double), NotUsed]] =
    items.map(item => yahooData(item, "close", "2015-01-01".toISODate(), "2016-01-31".toISODate()))

  def printSink(): () => Sink[(ISODate, Double), Future[Done]] = () => Sink.foreach[(ISODate, Double)](p => println(p))

}
