package kic.pipeline.sinks

import java.util.concurrent.LinkedBlockingQueue

import akka.Done
import akka.stream.scaladsl.Sink

import scala.concurrent.Future

case class PlotEvent()

object PlotEventSink {
  var states = scala.collection.mutable.Map[String, LinkedBlockingQueue[PlotEvent]]()

  def apply(id: String): Sink[PlotEvent, Future[Done]] = {
    val q: LinkedBlockingQueue[PlotEvent] = new LinkedBlockingQueue[PlotEvent]()
    Sink.foreach[PlotEvent](pe => q.offer(pe))
  }

  def subscribe(id: String): Unit = {

  }

}

object Foo {
  def main(args: Array[String]): Unit = {
    val x = PlotEventSink("dasdas")

  }
}
