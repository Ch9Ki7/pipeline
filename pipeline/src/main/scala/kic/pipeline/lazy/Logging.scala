package kic.pipeline.`lazy`

/**
  * Created by kindler on 18/11/2016.
  */
trait Logging {
  // later we can use any logger here that we want like logback or slf4j or akka's logger
  // since scala has no statics but companion or singleton objects the hascode makes a lot of sense
  // i.e. you could query for it in a heap dump!
  private def logger(level:String, msg: Any) = println(s"${this.getClass.getName}@${this.hashCode()}: $level: $msg")

  def error(msg: Any): Unit = logger("ERROR", msg)
  def warn(msg: Any): Unit  = logger(" WARN", msg)
  def info(msg: Any): Unit  = logger(" INFO", msg)
  def debug(msg: Any): Unit = logger("DEBUG", msg)
  def trace(msg: Any): Unit = logger("TRACE", msg)
}
