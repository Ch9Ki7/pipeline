package kic.pipeline.sources

import java.net.URL

import akka.stream.scaladsl.Source
import kic.pipeline.Data.debug
import kic.pipeline.ISODate
import org.joda.time.LocalDate
import kantan.csv

import scala.collection.mutable.ListBuffer
import kantan.csv.ops._
import kantan.csv.joda.time._
import com.github.nscala_time.time.OrderingImplicits._  // implicit ordering for joda date / time classes
import kantan.csv.generic._ // case class decoder derivation

/**
  * Created by kindler on 10/01/2017.
  */
case class YahooBar(date: LocalDate, open: Double, high: Double, low: Double, close: Double, vol: Double, adjClose: Double)

case class YahooSource(symbol: String, indicator: String, fromDate: ISODate, toDate: ISODate) {
  def fetch() = {
    val name = symbol+ "::" + indicator
    val indicatorIdx = Map[String, Int]("OPEN" -> 1, "HIGH" -> 2, "LOW" -> 3, "CLOSE" -> 4, "VOLUME" -> 5, "ADJ-CLOSE" -> 6)
    val frmMonth_1 = fromDate.month - 1
    val frmDay = fromDate.day
    val frmYear = fromDate.year
    val toMonth_1 = toDate.month - 1
    val toDay = toDate.day
    val toYear = toDate.year

    // http://chart.finance.yahoo.com/table.csv?s=AA&a=0&b=2&c=1970&d=10&e=8&f=2016&g=d&ignore=.csv
    val path = "/table.csv"
    val queryString = s"s=$symbol&a=$frmMonth_1&b=$frmDay&c=$frmYear&d=$toMonth_1&e=$toDay&f=$toYear&g=d&ignore=.csv"
    val url = "http://chart.finance.yahoo.com" + path + "?" + queryString

    debug(url)
    val csvReader = new URL(url).asCsvReader[YahooBar](',', true)

    Source.fromIterator[(LocalDate, Double)](() => new Iterator[(LocalDate, Double)] {
      val barsIt = csvReader.toStream.reverseIterator

      override def hasNext: Boolean = barsIt.hasNext

      override def next(): (LocalDate, Double) = {
        val n: YahooBar = barsIt.next().get
        indicator match {
          case "open" => (n.date, n.open)
          case "high" => (n.date, n.high)
          case "low" => (n.date, n.low)
          case "close" => (n.date, n.close)
          case "vol" => (n.date, n.vol)
          case "adjClose" => (n.date, n.adjClose)
          case _ =>   (n.date, n.adjClose)
        }
      }
    }
    ).named(name)
  }
}
