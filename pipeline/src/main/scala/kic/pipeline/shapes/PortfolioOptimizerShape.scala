package kic.pipeline.shapes

import akka.stream.{Inlet, Outlet, Shape}
import kic.pipeline.implicits.Types.Matrix
import kic.pipeline.stages.Weights

import scala.collection.immutable.Seq

case class PortfolioOptimizerShape[K: Ordering, V: Fractional](
                                                                in0ExpectedReturns: Inlet[(K, Matrix)],
                                                                in1ObservedReturns: Inlet[(K, Matrix)],
                                                                in2Covariances: Inlet[(K, Matrix)],
                                                                in3BenchmarkReturns: Inlet[(K, V)],

                                                                out0Weights: Outlet[(K, Weights)],
                                                                out1Rebalanced: Outlet[(K, Boolean)],
                                                                out2PortfolioRisk: Outlet[(K, V)],
                                                                out3PortfolioReturn: Outlet[(K, V)],
                                                                out4BenchmarkReturn: Outlet[(K, V)],
                                                                out5Alpha: Outlet[(K, V)],
                                                                out6OneOverNRisk: Outlet[(K, V)],
                                                                out7OneOverNReturn: Outlet[(K, V)]
) extends Shape {

  override def inlets: Seq[Inlet[_]] = List(in0ExpectedReturns, in1ObservedReturns, in2Covariances, in3BenchmarkReturns)

  override def outlets: Seq[Outlet[_]] = List(out0Weights, out1Rebalanced, out2PortfolioRisk, out3PortfolioReturn, out4BenchmarkReturn, out5Alpha, out6OneOverNRisk, out7OneOverNReturn)

  override def deepCopy(): Shape =
    new PortfolioOptimizerShape[K, V](
      in0ExpectedReturns.carbonCopy(), in1ObservedReturns.carbonCopy(), in2Covariances.carbonCopy(), in3BenchmarkReturns.carbonCopy(),
      out0Weights.carbonCopy(), out1Rebalanced.carbonCopy(), out2PortfolioRisk.carbonCopy(), out3PortfolioReturn.carbonCopy(), out4BenchmarkReturn.carbonCopy(), out5Alpha.carbonCopy(), out6OneOverNRisk.carbonCopy(), out7OneOverNReturn.carbonCopy()
    )

  override def copyFromPorts(inlets: Seq[Inlet[_]], outlets: Seq[Outlet[_]]): Shape =
    new PortfolioOptimizerShape[K, V](
      inlets(0).as[(K, Matrix)], inlets(1).as[(K, Matrix)], inlets(2).as[(K, Matrix)], inlets(3).as[(K,V)],
      outlets(0).as[(K, Weights)], outlets(1).as[(K, Boolean)], outlets(2).as[(K, V)], outlets(3).as[(K, V)], outlets(4).as[(K, V)], outlets(5).as[(K, V)], outlets(6).as[(K, V)], outlets(7).as[(K, V)]
    )
}