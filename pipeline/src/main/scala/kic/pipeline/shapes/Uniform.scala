package kic.pipeline.shapes

import akka.stream.{Inlet, Outlet, Shape}

import scala.collection.immutable.Seq

private object CopyHelper {
  def copyFromPorts[T](shape: Shape, f: (Seq[Inlet[T]], Seq[Outlet[T]]) => Shape): Shape =
    f(shape.inlets.asInstanceOf[Seq[Inlet[T]]], shape.outlets.asInstanceOf[Seq[Outlet[T]]])
}

case class UniformNInMOutShape[T](inlets: Seq[Inlet[T]], outlets: Seq[Outlet[T]]) extends Shape {
  override def deepCopy() = UniformNInMOutShape(inlets.map(_.carbonCopy()), outlets.map(_.carbonCopy()))
  override def copyFromPorts(inlets: Seq[Inlet[_]], outlets: Seq[Outlet[_]]): Shape =
    UniformNInMOutShape[T](inlets.asInstanceOf[Seq[Inlet[T]]], outlets.asInstanceOf[Seq[Outlet[T]]])
}

case class UniformNInNOutShape[T](inlets: Seq[Inlet[T]], outlets: Seq[Outlet[T]]) extends Shape {
  require(inlets.length == outlets.length)

  override def deepCopy() = UniformNInNOutShape(inlets.map(_.carbonCopy()), outlets.map(_.carbonCopy()))
  override def copyFromPorts(inlets: Seq[Inlet[_]], outlets: Seq[Outlet[_]]): Shape =
    UniformNInNOutShape[T](inlets.asInstanceOf[Seq[Inlet[T]]], outlets.asInstanceOf[Seq[Outlet[T]]])
}

case class Uniform1InMOutShape[T](in: Inlet[T], override val outlets: Seq[Outlet[T]]) extends Shape {
  override val inlets = List(in)
  override def deepCopy(): Shape = copyFromPorts(inlets.map(_.carbonCopy()), outlets.map(_.carbonCopy()))
  override def copyFromPorts(inlets: Seq[Inlet[_]], outlets: Seq[Outlet[_]]): Shape =
    CopyHelper.copyFromPorts[T](this, (i, o) => Uniform1InMOutShape(i(0), o))
}
