package kic.pipeline.shapes

import akka.stream.{Inlet, Outlet, Shape, SinkShape}
import scala.annotation.unchecked.uncheckedVariance
import scala.collection.immutable

/**
  * Created by kindler on 25/01/2017.
  */

case object EmptyImmutableSeq extends immutable.Seq[Nothing] {
  override final def iterator = Iterator.empty
  override final def apply(idx: Int): Nothing = throw new java.lang.IndexOutOfBoundsException(idx.toString)
  override final def length: Int = 0
}

final case class ZipSinkShape[-T1, -T2](in0: Inlet[T1 @uncheckedVariance], in1: Inlet[T2 @uncheckedVariance]) extends Shape {
  override val inlets: immutable.Seq[Inlet[_]] = List(in0, in1)
  override val outlets: immutable.Seq[Outlet[_]] = EmptyImmutableSeq

  override def deepCopy(): ZipSinkShape[T1, T2] = ZipSinkShape(in0.carbonCopy(), in1.carbonCopy())

  override def copyFromPorts(inlets: immutable.Seq[Inlet[_]], outlets: immutable.Seq[Outlet[_]]): Shape = {
    require(inlets.size == 2, s"proposed inlets [${inlets.mkString(", ")}] do not fit SinkShape")
    require(outlets.isEmpty, s"proposed outlets [${outlets.mkString(", ")}] do not fit SinkShape")
    ZipSinkShape(inlets(0), inlets(1))
  }
}


