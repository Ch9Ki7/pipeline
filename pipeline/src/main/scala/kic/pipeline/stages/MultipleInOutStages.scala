package kic.pipeline.stages

import java.util.Comparator

import akka.stream._
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import kic.pipeline.`lazy`.Logging
import kic.pipeline.utils._

import scala.collection.immutable
import scala.collection.immutable.Seq
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

case class TakeXThenStreamShape[K, V](in: Inlet[(K, V)], out0: Outlet[(K, Seq[V])], out1: Outlet[(K, V)]) extends Shape {
  override val inlets: immutable.Seq[Inlet[_]] = List(in)
  override val outlets: immutable.Seq[Outlet[_]] = List(out0, out1)
  override def deepCopy() = TakeXThenStreamShape(in.carbonCopy(), out0.carbonCopy(), out1.carbonCopy())
  override def copyFromPorts(inlets: Seq[Inlet[_]], outlets: Seq[Outlet[_]]): Shape =
    TakeXThenStreamShape[K, V](inlets(0).asInstanceOf[Inlet[(K, V)]], outlets(0).asInstanceOf[Outlet[(K, Seq[V])]], outlets(1).asInstanceOf[Outlet[(K, V)]])
}

class TakeXThenStream[K, V](takeFirst: Int) extends GraphStage[TakeXThenStreamShape[K, V]] {
  require(takeFirst > 0)

  override val shape: TakeXThenStreamShape[K, V] =
    TakeXThenStreamShape(Inlet[(K, V)]("take_then_stream_in"), Outlet[(K, Seq[V])]("first_n"), Outlet[(K, V)]("stream_rest"))

  @scala.throws[Exception](classOf[Exception])
  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape)  {
    private val lastIdx = takeFirst - 1
    private var firstN:Seq[V] = Vector.empty[V]
    private var pulled = false
    private var i = 0

    setHandler(shape.in, new InHandler {
      @scala.throws[Exception](classOf[Exception])
      override def onPush(): Unit = {
        val elemIn = grab(shape.in)

        if (i < lastIdx) {
          firstN :+= elemIn._2
          tryPull(shape.in)
        } else if (i < takeFirst) {
          firstN :+= elemIn._2
          push(shape.out0, (elemIn._1, firstN))
          tryPull(shape.in)
        } else {
          push(shape.out1, elemIn)
        }

        i += 1
      }

      @scala.throws[Exception](classOf[Exception])
      override def onUpstreamFinish(): Unit = completeStage()
    })

    setHandler(shape.out0, new OutHandler {
      @scala.throws[Exception](classOf[Exception])
      override def onPull(): Unit =  {
        if (i < 1) {
          tryPull(shape.in)
        } else {
          complete(shape.out0)
        }
      }
    })

    setHandler(shape.out1, new OutHandler {
      @scala.throws[Exception](classOf[Exception])
      override def onPull(): Unit = {
        if (i >= takeFirst) {
          tryPull(shape.in)
        }
      }
    })
  }
}