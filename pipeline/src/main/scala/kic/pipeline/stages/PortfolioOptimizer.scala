package kic.pipeline.stages

import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import kic.pipeline.`lazy`.Logging
import kic.pipeline.implicits.Types.Matrix
import kic.pipeline.utils.RiskReturnOptimizer
import kic.pipeline.implicits.Types.convertSeqNumericToColumnVector

/**
  * This stage is doing the portfolio optimization. You need to make sure that all the inlets are synconized and
  * provided in the correct order. Therefore do not use this class directly but over the Quant#optimizePortfolio()
  * function!
  *
  * Also see doc on stages:
  * http://doc.akka.io/docs/akka/2.4.12/scala/stream/stream-customize.html#Custom_processing_with_GraphStage
  */

case class Weights(var rebalanced: Boolean, positions: Seq[(String, Double)], cash: Double) {
  def onlyCash(): Boolean = cash >= 0.99
  def toMap(): Map[String, Double] = (positions ++ List(("CASH", cash))).toMap
  def weights(): Array[Double] = (positions ++ List(("CASH", cash))).map({case (s, w) => w}).toArray[Double]
}

case class PortfolioOptimizerInput[K, V] (key: K, expectedReturns: Matrix, observedReturns: Matrix, covariances: Matrix, benchmarkReturn: V)
case class PortfolioOptimizerOutput[K, V] (key: K, weights: Weights, rebalanced: Boolean, portfolioRisk: V, portfolioReturn: V, benchmarkReturn: V, alpha: V, oneOverNRisk: V, oneOverNReturn: V)

class PortfolioOptimizer[K: Ordering, V: Fractional](symbols: Seq[String], riskAversion0To100: Int, stopLossStdDevs: Double, fromDouble: Double => V)(implicit fractional: Fractional[V]) extends GraphStage[FlowShape[PortfolioOptimizerInput[K, V], PortfolioOptimizerOutput[K, V]]] {

  case class Performance(size: Int) extends Logging {
    private val perf: Array[Double] = Array.fill[Double](size)(0d)
    private val perfHh: Array[Double] = Array.fill[Double](size)(0d)

    def isStoppedOutNow(covariances: Matrix, observedReturns: Matrix, stdDevs: Double = 2.0): Boolean = {
      var stoppedOut: Boolean = false

      for (i <- 0 until size) {
        perf(i) = (1d + perf(i)) * (1d + observedReturns.getElement(i, 0)) -1d
        perfHh(i) = math.max(perf(i), perfHh(i))

        val stopLossRisk = math.sqrt(covariances.getElement(i , i)) * stdDevs
        val lowerBound = perfHh(i) - stopLossRisk

        if (perf(i) < lowerBound) {
          info(s"position $i got stopped out ${perf(i)} < $lowerBound (${perfHh(i)} - $stopLossRisk)")
          perf(i) = 0d
          perfHh(i) = 0d
          stoppedOut = true
        }
      }

      stoppedOut
    }
  }

  override val shape: FlowShape[PortfolioOptimizerInput[K, V], PortfolioOptimizerOutput[K, V]] =
    new FlowShape(Inlet[PortfolioOptimizerInput[K, V]]("optimizer-input"), Outlet[PortfolioOptimizerOutput[K, V]]("optimizer-output"))

  @scala.throws[Exception](classOf[Exception])
  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) with OutHandler with InHandler {
    private val size = symbols.length
    private var weightsInclCash = Weights(false, symbols.map(s => (s, 0d)), 1d) // (0 until size + 1, "CASH", 0d).map(i => if (i < size) 0d else 1d).toArray[Double]
    private var weightsM1:  Matrix = (0 until size).map(x => 0d)
    val performance: Performance = Performance(size)
    val oONweightsM1: Matrix = (0 until size).map(x => 1d / size)
    val oONweightsM1Transpose = oONweightsM1.transpose
    val minPosSize = 0.01


    // set the in and out handlers
    setHandler(shape.in, this)
    setHandler(shape.out, this)

    // IN handler
    @scala.throws[Exception](classOf[Exception])
    override def onPush(): Unit = {
      val elem = grab(shape.in)

      // 1. calculate the risk and return of the current weights
      val pRisk = (weightsM1.transpose * elem.covariances * weightsM1).getElement(0, 0)
      val pReturn = (weightsM1.transpose * elem.observedReturns).getElement(0, 0)

      // 2. calculate the performance and the highest high of the performance and check if we got stopped out
      //    this is what we realize (note we start with a 0 weight vector, on the 2st day we earn nothing)
      val stoppedOut = performance.isStoppedOutNow(elem.covariances, elem.observedReturns, stopLossStdDevs)

      // 3. if we got stopped out or if we only invested in the risk free fall back then optimize for new weights
      val rebalance = stoppedOut || weightsInclCash.onlyCash() // weightsInclCash(cashIdx) >= 0.99
      if (rebalance) {
        val weights: Array[Double] = RiskReturnOptimizer(elem.covariances, elem.expectedReturns).optimize(riskAversion0To100)
        weightsInclCash = Weights(true, symbols.zip(weights), weights.last) //FIXME rebalance should only be true if there is a difference to the last weights!
        weightsM1 = (0 until size).map(i => weights(i))
      } else {
        weightsInclCash.rebalanced = false
      }

      // calculate the 1/N reference portfolio, this is mainly used for testing or as another benchmark
      val oONRisk = oONweightsM1Transpose * elem.covariances * oONweightsM1
      val oONReturn = oONweightsM1Transpose * elem.observedReturns

      // finally push our results
      push(shape.out, PortfolioOptimizerOutput[K,V](
        elem.key, weightsInclCash, rebalance, fromDouble(pRisk), fromDouble(pReturn), elem.benchmarkReturn,
        fromDouble(0d), fromDouble(oONRisk.getElement(0,0)), fromDouble(oONReturn.getElement(0,0))))
    }

    // OUT handler
    @scala.throws[Exception](classOf[Exception])
    override def onPull(): Unit = {
      pull(shape.in)
    }
  }
}
