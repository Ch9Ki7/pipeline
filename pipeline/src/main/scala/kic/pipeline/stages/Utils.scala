package kic.pipeline.stages

import java.util
import java.util.Comparator

import scala.annotation.tailrec
import scala.collection.mutable

final class Comparators[K: Ordering] {
  val TuplComarator = new Comparator[(K,_)] {
    override def compare(o1: Tuple2[K, _], o2: Tuple2[K, _]): Int = Ordering[K].compare(o1._1, o2._1)
  }
}

object Comparators {
  def apply[K: Ordering]: Comparators[K] = new Comparators[K]()
}

case class ColumnBuffer[T](maxBufferSize: Int, comparator: Comparator[T]) {
  require(maxBufferSize > 2)
  private var isClosed = false
  val maxHead = 1
  val head = new util.LinkedList[T]()
  val tail = new util.LinkedList[T]()

  def add(element:T): Unit = {
    if (head.size() < maxHead) {
      head.add(element)
    } else {
      tail.add(element)
    }
  }

  def isEmpty(): Boolean = !hasHead()

  def hasHead(): Boolean = head.size() > 0

  def getHead(): T = head.getFirst

  def getFirstOfTail(): T = tail.getFirst

  def hasDemand(): Boolean = length() < maxBufferSize && !isClosed

  def hasTailData(): Boolean = tail.size() > 0

  def close(): Unit = isClosed = true

  def hasValidHead(reference: T): Boolean =
    tail.size() > 0 && comparator.compare(tail.getFirst(), reference) > 0 ||
    isClosed && head.size() > 0 && tail.size() < 0 && comparator.compare(head.getFirst, reference) <= 0

  def hasInvalidHead(reference: T): Boolean = !hasValidHead(reference)

  def hasTailDataButInvalidHead(reference: T): Boolean = hasTailData() && hasInvalidHead(reference)

  def dropFirst(): Boolean = {
    head.pollFirst()
    head.add(tail.pollFirst())
    if (tail.size() < maxBufferSize) {
      true;
    } else {
      false;
    }
  }

  def poll(): T = {
    if (tail.size() > 0) head.add(tail.pollFirst())
    head.pollFirst()
  }

  def closed() = isClosed

  def length() = head.size() + tail.size()

  override def toString: String = head.toString + " " + tail.toString
}

// FIXME we need to clean this class up a little bit - this is just the first time everything works
// TODO I guess we can use an implicit comparator somehow instead of the required constructor argument - see also kic.pipeline.implicits.Types
case class SynchronizedBuffers[T](name: String, nrOfColumns: Int, puller: Int => Unit, comparator: Comparator[T], maxBufferSize: Int = 5) {
  require(maxBufferSize > 2)
  private val dummyCallback = () => {}
  private val outQueue = Array.fill[() => Unit](nrOfColumns)(dummyCallback)
  private val columns = (0 until nrOfColumns).map(i => ColumnBuffer[T](maxBufferSize, comparator))
  private val orderer = new util.TreeSet[T](comparator)
  private val portCheck = mutable.HashSet[Int]()
  private val portDone = mutable.HashSet[Int]()
  private val front = mutable.HashMap[Int, T]()
  private val empty = mutable.HashMap[Int, T]()
  private var row = mutable.HashMap[Int, T]()

  def close(col: Int): Boolean = {
    // close column
    columns(col).close()

    // signal port availability
    portCheck.add(col)

    // if we close an empty column we can complete the stage immediately
    if (!columns(col).hasHead() && !row.get(col).isDefined){
      return true
    } else {
      // call outbounds
      outQueue.foreach(_ ())
      return false
    }
  }

  def put(col: Int, elem: T): Unit = {
    val column = columns(col)

    //println(s"$name: put on $col, $elem")

    // put the very first ever added element to the front
    if (!column.hasHead()) orderer.add(elem)

    // append the element to the buffer
    column.add(elem)

    // keep track of ports with valid data
    if (column.hasTailData()) portCheck.add(col)

    // call outbounds
    outQueue.foreach(_())
  }

  private def hasDemand(cIdx: Int): Boolean = {
    val col = columns(cIdx)

    if (col.hasDemand()) {
      return true
    } else {
      // it can happen that a buffer is full but we never got anything on any other buffer.
      // at this point we do not know if this is just a very slow source or if we filtered away more "emits"
      // than the size of this buffer.

      if (portCheck.size < nrOfColumns) {
        // in this case we need to keep filling up this buffer even if it is above its capacity
        return !col.closed()
      } else {
        // otherwise we have data
        return false
      }
    }
  }

  @tailrec
  private def tryGetFrontRow(): mutable.HashMap[Int, T] = {
    if (portCheck.size >= nrOfColumns && orderer.size() > 0) {
      // we know that there is at least one column starting with this value so we can safely poll it
      val key = orderer.pollFirst()

      for (cIdx <- 0 until nrOfColumns) {
        val col = columns(cIdx)

        // check if we have a valid value in the column
        if (col.hasHead() && comparator.compare(col.getHead(), key) <= 0) {
          front.put(cIdx, col.poll())
          if (col.hasHead()) orderer.add(col.getHead())
        }

        // if we do not have further data then make port unavailable until we get more data
        if (!col.closed() && !col.hasTailData())
          portCheck.remove(cIdx)

        // indicate that this port is done
        if (!col.hasHead() && col.closed())
          portDone.add(cIdx)
      }

      if (front.size >= nrOfColumns) {
        front.clone()
      } else {
        //println(s"$key")
        tryGetFrontRow()
      }
    } else {
      // we do not have enough data on all ports (portCheck.size < nrOfColumns) so we return empty
      empty
    }
  }

  def consume(idx: Int, callback: T => Unit): Unit = {
    val column = columns(idx)

    // if front.size <= 0 then makeFrontRow
    row = if (row.size <= 0 && !complete()) tryGetFrontRow() else row

    // if front.remove(idx).ispresent then return that value otherwise stall a callback
    val elem = row.remove(idx)
    if (elem.isDefined) {
      // we can for sure emit the element
      // println(s"$name: push on $idx, $elem")
      callback(elem.get)
      // println(s"$idx $elem")

      // disable callback and wait for new consumption request
      outQueue(idx) = dummyCallback

      // call outbounds
      if (row.size > 0) outQueue.foreach(_())
    } else {
      // no data available yet, just wait for being called back by the puller
      outQueue(idx) = () => consume(idx, callback)
    }

    // whatever happens pull for more data (if we still have demand)
    if (hasDemand(idx)) puller(idx)
  }

  def complete(): Boolean = portDone.size >= nrOfColumns && row.size <= 0
}