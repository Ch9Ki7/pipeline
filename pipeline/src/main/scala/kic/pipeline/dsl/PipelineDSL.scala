package kic.pipeline.dsl

import akka.NotUsed
import akka.stream.{FlowShape, Graph}
import akka.stream.scaladsl.{Flow, GraphDSL, Source}
import kic.pipeline.{Data, ISODate}
import kic.pipeline.flows.Quant
import kic.pipeline.shapes.Uniform1InMOutShape

import scala.collection.mutable

/**
  * Created by kic on 27.12.16.
  */
object PipelineDSL {
  /* Add following DSL features

    "sequece of sources" connection to a "function providing a flow" Seq[Source[_]] ~> () -> Flow[_]
    "function providing a flow" connection to a "function providing a flow" () -> Flow[_] ~> () -> Flow[_]
    "function providing a flow" connection to a "function providing a sink" () -> Flow[_] ~> () -> Sink[_]
    tpul of (Inlet[_], outlet[_]) sould also be a possible DSL source and target like ~> (macd.in, macd.histogram) ~>
    There are also some nice to haves like: by default connect any outlet to an "ingore sink" so the user is not forced to use all ever provided outputs i.e. if someone does not want to plot the alpha output from the optimizer flow.
    automatically put a Broadcast flow between elements as soon as an outlet is wired more then once like if x ~> y; x ~> z then automatically generate x ~> bcast(2); bcast.out(0) ~> y; bcast.out(1) ~> z
  */

  /*  NOTES
   *  =====
   *
   *  We could use ~>: as a right assigned fan in operator
   *
   */

  implicit class FlowDSL[I, O, Mat](flows: Seq[Flow[I, O, Mat]]) {
    def ~>[I2, O2, Mat] (flow: () => Flow[I2, O2, Mat]): Seq[Flow[I2, O2, Mat]] = ???
  }

  implicit class FromSource[I](sources: Array[Source[I, NotUsed]]) {
    def foo(): Unit = {}
    def ~>[I, O, Mat] (flow: () => Flow[I, O, Mat]) = {

    }
  }


  trait DSL[I] {
    val foo = "bar"

    def ~>[O1, O2] (x: DSL[O1], y: DSL[O2]): DSL[I] = this

    def ~>[O] (x: DSL[O]) = x

    def ~> (x: Uniform1InMOutShape[I]) = ???  // if a shape has multiple outlets return a function which accepts a number of dsl's like the broadcast is doing, we could do a generic function like (out0: XY = ignore, ...)

    def ~> (i: Int) = 99
  }

  implicit class FromSource2[I](sources: Array[Source[I, NotUsed]]) extends DSL[I]
  implicit class FromFlow[I, O](f: () => Flow[I, O, NotUsed]) extends DSL[I]

  def main(args: Array[String]): Unit = {
    val symbols = Array("AAPL", "MSFT", "GOOG", "AMZN")
    val startDate = new ISODate("2015-01-01")
    val endDate =  new ISODate("2016-01-31")

    val sources = symbols.map(Data.yahooData(_, "close", startDate, endDate))
    val qt = Quant[ISODate, Double]

    //new xxx(qt.price2Return) ~> 2 ~> 2
    // qt.price2Return ~> 2

    sources ~> 2
    // sources   ~> (qt.price2Return,2)

    val dsl = new FromSource2[(ISODate, Double)](sources)
    qt.price2Return() ~> qt.price2Return() ~> qt.price2Return() ~> qt.price2Return() ~> qt.return2Performance() ~> qt.price2Return()

    dsl ~> qt.price2Return() ~> (
                                   qt.price2Return() ~> qt.return2Performance() // ~> qt.macd(1,2,0)
                                  ,qt.price2Return()
                                )

    /* TODO this is how it should look like (as close as possible)

        shelf     ~> price2Return ~> macd(12, 26, 9).histogram ~> fanIntoColumnVector    ~> optimizer.in0ExpectedReturns                    // shelf is a sequence of sources* and ignore unused macd outlets
                                  ~>                                                        optimizer.in1ObservedReturns                    // automatically boradcast returns
                                  ~>                              fanIntoCovariance(100) ~> optimizer.in2Covariances                        // automatically boradcast returns
        benchmark ~> price2Return ~>                                                        optimizer.in3BenchmarkReturns                   // operate an single sources as well
                                                                                            optimizer.out2PortfolioReturn  ~> plot(line)    // ignore the other two outlets by default


        *) or just symbols maybe even better?



      To compare this is how we need to do it by today - this is far to verbose:
            val g = RunnableGraph.fromGraph(GraphDSL.create(sink[Any]()) { implicit b => sink =>
              import GraphDSL.Implicits._

              val cov = b.add(qt.fanIntoCovariance(rawSources.length, 6))
              val obsRet = b.add(qt.fanIntoRowVector(rawSources.length))
              val expRet = b.add(qt.fanIntoRowVector(rawSources.length))
              val bench = b.add(Source(tsSp5))
              val benchToRet = b.add(qt.price2Return()())
              val opt = b.add(qt.optimizePortfolio())

              rawSources.indices.map(i => {
                val src = b.add(Source(rawSources(i)))
                val toRet = b.add(qt.price2Return()())
                val bcRet = b.add(Broadcast[E](3))
                val toPerf = b.add(qt.return2Performance()())
                val macd = b.add(qt.macd(3, 5, 0, qt.sma)())


                src ~> toRet ~> bcRet.in
                                bcRet.out(0) ~> cov.in(i)
                                bcRet.out(1) ~> obsRet.in(i)
                                bcRet.out(2) ~> toPerf ~> macd.in
                                                          macd.outlets(0) ~> expRet.in(i)
                                                          macd.outlets(1) ~> Sink.ignore
                                                          macd.outlets(2) ~> Sink.ignore
              })

              expRet.out               ~> opt.in0ExpectedReturns
              obsRet.out               ~> opt.in1ObservedReturns
              cov.out                  ~> opt.in2Covariances
              bench      ~> benchToRet ~> opt.in3BenchmarkReturns
                                          opt.out0PortfolioRisk     ~> Sink.ignore
                                          opt.out1PortfolioReturn   ~> Sink.ignore
                                          opt.out2Alpha             ~> sink

        */


  }
}


