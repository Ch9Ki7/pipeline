package kic.pipeline.flows

import akka.stream._
import akka.stream.scaladsl.{Broadcast, Concat, Flow, GraphDSL, Source}


object FeedbackLoop {

  def apply[T, Mat](shape: Graph[Shape, Mat], startValue: T): Flow[T, T, Mat] = apply(shape, 0, 1, 0, startValue)

  def apply[T, Mat](shape: Graph[Shape, Mat], feedBackInletIdx: Int, dataInletIdx: Int, ouletIdx: Int, startValue: T): Flow[T, T, Mat] = Flow.fromGraph(
    GraphDSL.create(shape) { implicit b => s =>
      import GraphDSL.Implicits._

      val start = b.add(Source.single[T](startValue))
      val concat = b.add(Concat[T]())
      val duplicateResult = b.add(Broadcast[T](2))

      // first we wire the inputs together
      start                                   ~> concat
      duplicateResult.out(feedBackInletIdx)   ~> concat ~> s.inlets(0).as[T]

      // we need to duplicate the output for the feedback
      s.outlets(ouletIdx).as[T] ~> duplicateResult

      // finaly return the flow shape with the two un-wired ports (one inlet and one of the duplicated outlets)
      FlowShape(s.inlets(dataInletIdx).as[T], duplicateResult.out(1))
    }
  ).named("feedback loop")

}
