package kic.pipeline.flows

import akka.stream.scaladsl.{GraphDSL, ZipWith}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import akka.stream.{Graph, _}
import kic.pipeline.shapes.UniformNInNOutShape
import kic.pipeline.stages.SynchronizedBuffers

class Synchronizer[T: Ordering](name: String, nrOfSources: Int, maxBufferSize: Int, ord: Ordering[T]) extends GraphStage[UniformNInNOutShape[T]] {
  override val shape: UniformNInNOutShape[T] = UniformNInNOutShape[T](
    (0 until nrOfSources).map(i => Inlet[T]("synchronize_in_" + i)),
    (0 until nrOfSources).map(i => Outlet[T]("synchronize_out_" + i))
  )

  @scala.throws[Exception](classOf[Exception])
  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) {
    private val puller = (port: Inlet[T]) => if (!hasBeenPulled(port)) tryPull(port)
    private val syncer = new SynchronizedBuffers[T](name, shape.inlets.length, (portIdx) => puller(shape.inlets(portIdx)), ord, maxBufferSize)


    // set in-Handlers
    shape.inlets.indices.foreach(i => setHandler(shape.inlets(i), new InHandler {
      val id = name + "-" + i   // useful for conditional breakpoints

      @scala.throws[Exception](classOf[Exception])
      override def onPush() = syncer.put(i, grab(shape.inlets(i)))

      @scala.throws[Exception](classOf[Exception])
      override def onUpstreamFinish(): Unit = if (syncer.close(i)) completeStage()  // complete stage if we closed an empty column

      @scala.throws[Exception](classOf[Exception])
      override def onUpstreamFailure(ex: Throwable): Unit = super.onUpstreamFailure(ex)
    }))

    // set out-Handlers
    shape.outlets.indices.foreach(i => setHandler(shape.outlets(i), new OutHandler {
      val id = name + "-" + i   // useful for conditional breakpoints

      @scala.throws[Exception](classOf[Exception])
      override def onPull() = {
        syncer.consume(i, elem => push(shape.outlets(i), elem))
        if (syncer.complete()) completeStage()
      }
    }))

    @scala.throws[Exception](classOf[Exception])
    override def preStart(): Unit = shape.inlets.foreach(pull(_))
  }
}

object Synchronizer {

  def apply[T](name: String, nrOfSources: Int, maxBufferSize: Int = 50)(implicit ord: Ordering[T]) =
    new Synchronizer[T](name, nrOfSources, Math.max(maxBufferSize, 5), ord)

}

object SignalOp {

  def apply[T: Ordering](op: (T, T) => T): Graph[FanInShape2[T, T, T], _] =
    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val sync = b.add(Synchronizer[T]("signal-operation", 2))
      val zipOp = b.add(ZipWith[T, T, T](op))

      // first of all we need to synchronize the signals
      sync.outlets(0) ~> zipOp.in0
      sync.outlets(1) ~> zipOp.in1

      new FanInShape2(sync.inlets(0), sync.inlets(1), zipOp.out)
    }

}
