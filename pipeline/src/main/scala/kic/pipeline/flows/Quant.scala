package kic.pipeline.flows

import akka.{Done, NotUsed}
import akka.stream._
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Sink, Source, UnzipWith, Zip, ZipWith, ZipWithN}
import kic.pipeline.implicits.Types._
import kic.pipeline.javainterfaces.Plotter
import kic.pipeline.shapes._
import kic.pipeline.stages.{PortfolioOptimizer, PortfolioOptimizerInput, PortfolioOptimizerOutput, Weights}

import scala.concurrent.Future


/**
  * Check this out! this is far more compact and readable then the QuantFlows.scala version.
  * I really start to enjoy scala ...
  */
object Quant {
  def apply[K: Ordering, V: Fractional]: Quant[K, V] = new Quant[K, V]()
}

/**
  *
  * @tparam K   Key Type of the time series
  * @tparam V   Value Type of the time series
  */
class Quant[K: Ordering, V: Fractional] {
  //import kic.pipeline.implicits.Conversions.tupleOrdering // FIXME we need to move this import into the localest scopes as possible

  // define an implicit numeric tupl which we can use to get zeros and ones and so on
  implicit val iNnum: Numeric[NumericEvent[K, V]] = (null.asInstanceOf[K], null.asInstanceOf[V])

  // do not force the api user to import all the implicits
  val asRowVector = AsRowVector
  val asColumnVector = AsColumnVector

  // shorten (Key, Value) as E (Event or Experiment)
  type E = (K, V)

  // use this case class to allow other function to only accept Moving Average Function
  case class MovingAverage[E](f: () => Flow[E, E, _]) extends (() => Flow[E, E, _]) {
    override def apply(): Flow[E, E, _] = f.apply()
  }

  // Simple math operations between two streams guaranties synchronisation between streams
  def plus() = SignalOp[E]((a, b) => a + b)(kic.pipeline.implicits.Conversions.tupleOrdering[K, V])
  def minus() = SignalOp[E]((a, b) => a - b)(kic.pipeline.implicits.Conversions.tupleOrdering[K, V])
  def divide() = SignalOp[E]((a, b) => a / b)(kic.pipeline.implicits.Conversions.tupleOrdering[K, V])
  def times() = SignalOp[E]((a, b) => a * b)(kic.pipeline.implicits.Conversions.tupleOrdering[K, V])


  /**
    *
    * @param chartId
    * @param plotter
    * @tparam V
    * @return
    */
  def plotFlow[V](chartId: String)(implicit plotter: Plotter): (String, String) => Flow[(K, V), (K, V), NotUsed] = {
    val plotterFact = plotSink[V](chartId)

    (dataSetId: String, plotType: String) => {
      plotter.onDataSet(dataSetId, plotType)

      Flow.fromGraph(GraphDSL.create() { implicit b =>
        import GraphDSL.Implicits._

        val bcast = b.add(Broadcast[(K, V)](2))
        val plot = b.add(plotterFact(dataSetId, plotType))

        bcast.out(1) ~> plot

        FlowShape[(K, V), (K, V)](bcast.in, bcast.out(0))
      })
    }
  }


  /**
    *
    * @param chartId
    * @param plotter
    * @tparam V
    * @return
    */
  def plotSink[V](chartId: String)(implicit plotter: Plotter): (String, String) => Sink[(K,V), Future[Done]] = {
    plotter.onChart(chartId)

    (dataSetId: String, plotType: String) => {
      plotter.onDataSet(dataSetId, plotType)
      Sink.foreach[(K, V)]({case (x, y) => plotter.plot(chartId, dataSetId, x, y)})
    }
  }


  /**
    *
    * @param chartId
    * @param plotter
    * @return
    */
  def plotSinkExtra(chartId: String)(implicit plotter: Plotter): (String, String) => Graph[ZipSinkShape[E, Map[_, _]], _] = {
    plotter.onChart(chartId)

    (dataSetId: String, plotType: String) => {
      plotter.onDataSet(dataSetId, plotType)

      GraphDSL.create() { implicit b =>
        import GraphDSL.Implicits._
        import scala.collection.JavaConverters._

        val zip = b.add(Zip[E, Map[_, _]]().named("plot extra zipper"))
        val plot = b.add(Sink.foreach[((K, V), Map[_, _])]({case ((x, y), ext) =>
          if (!ext.isEmpty) plotter.plot(chartId, dataSetId, x, y, ext.asJava)}).named("plot sink"))

        zip.out ~> plot

        ZipSinkShape[E, Map[_, _]](zip.in0, zip.in1)
      }
    }
  }


  /**
    * Converts a price time series into a returns time series
    *
    * @return     returns time series
    */
  def price2Return(window: Int = 2): () => Flow[E, E, NotUsed] =
    () => Flow[E].sliding(math.max(window, 2), math.max(window, 2) - 1).map(events => (events(events.length - 1) / events(0) - events(0).one))


  /**
    *
    * @return
    */
  def return2Performance(): () => Flow[E, E, NotUsed] =
    () => FeedbackLoop(ZipWith[E, E, E]((a, b) => ((a + a.one) * (b + b.one) - a.one)), (null.asInstanceOf[K], null.asInstanceOf[V]).zero.asTupl())


  /**
    *
    * @return
    */
  def price2Performance(): Flow[E, E, NotUsed] =
    price2Return()().via(return2Performance()())


  /**
    *
    * @param size
    * @return           single stream of vectors
    */
  def fanIntoColumnVector(size: Int): ZipWithN[E, (K, Matrix)] =
    ZipWithN[E, (K, Matrix)](event => (event(0)._1, event.toM1Matrix()))(size)


  def fanIntoRowVector(size: Int): ZipWithN[E, (K, Matrix)] =
    ZipWithN[E, (K, Matrix)](event => (event(0)._1, event.to1NMatrix()))(size)


  def fanIntoCovariance(size: Int, window: Int): Graph[UniformFanInShape[E, (K, Matrix)], _] = {
    val win = math.max(window, 2)
    val inletsRange = (0 until size)

    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val sync = b.add(Synchronizer[E]("cov", size, win * 2)(kic.pipeline.implicits.Conversions.tupleOrdering[K, V]).named(s"Cov($win): sync streams"))
      val demeanX = inletsRange.map(i => b.add(Flow[E].sliding(win, 1).map(row => row.deMean()).named(s"Cov($win): demean x")))
      val cov = b.add(ZipWithN[Seq[E], (K, Matrix)](event => (event(0).max(kic.pipeline.implicits.Conversions.tupleOrdering[K, V])._1, event.toColumnMatrix().transposeTimesItself() / (window - 1.0)))(size))

      inletsRange.foreach(i => sync.outlets(i) ~> demeanX(i) ~> cov.in(i))

      UniformFanInShape[E, (K, Matrix)](cov.out, sync.inlets:_*)
    }
  }


  def fanIntoCovarianceEWMA() = ???


  /**
    * converts a given series into a series of sign changes. in combination with a `minus`
    * this can be used as a crossover/-under indicator
    *
    * @return     -1 when the sign changed from positive to negative, 1 when the sign changed from negative to positive else 0
    */
  def signChanges() =
    Flow[E].sliding(2, 1).map(events => (events(events.length - 1).signChangeTo(events(0))))


  /**
    * Calculates a simple moving average
    *
    * @param window window size of the moving average
    * @return       a simple moving average
    */
  def sma(window: Int): MovingAverage[E] = {
    val win = math.max(window, 0)
    def zero() = Flow[E].map(e => e.zero.asTupl()).named(s"SMA(0)")
    def regular() = Flow[E].sliding(win, 1).map(rows => rows.avg).named(s"SMA($win)")

    if (win <= 0) {
      MovingAverage(zero)
    } else {
      MovingAverage(regular)
    }
  }

  // class MACDShape(in: Inlet[E], val out0Macd: Outlet[E], val out1Signal: Outlet[E], val out2Hist: Outlet[E]) extends Uniform1InMOutShape[E](in, List(out0Macd, out1Signal, out2Hist))
  def macd(fastWindow: Int = 12, slowWindow: Int = 26, signalWindow: Int = 9, fAvg: (Int) => MovingAverage[E] = sma): () => Graph[Uniform1InMOutShape[E], _] = {
    val fWin = math.max(fastWindow, 0)
    val sWin = math.max(slowWindow, fWin + 1)
    val gWin = math.max(signalWindow, 0)

    () => GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val bcPrice = b.add(Broadcast[E](2))
      val fast = b.add(fAvg(fWin)())
      val slow = b.add(fAvg(sWin)())
      val delta = b.add(minus())
      val bcMacd = b.add(Broadcast[E](3))
      val signal = b.add(fAvg(gWin)())
      val bcSig = b.add(Broadcast[E](2))
      val hist = b.add(minus())

      bcPrice.out(0) ~> fast ~> delta.in0
      bcPrice.out(1) ~> slow ~> delta.in1
                                delta.out ~> bcMacd.in
                                             bcMacd.out(0) ~>                                     hist.in0
                                             bcMacd.out(1) ~> signal ~> bcSig.in; bcSig.out(0) ~> hist.in1

      Uniform1InMOutShape[E](bcPrice.in, List(bcMacd.out(2), bcSig.out(1), hist.out))
      //new MACDShape(bcPrice.in, bcMacd.out(2), bcSig.out(1), hist.out)
    }
  }

  def optimizePortfolio(size: Int /*FXIME pass symbols instead of size*/, riskAversion1To100: Int = 50, stopLossStdDevs: Double = 2.0)(implicit fromDouble: Double => V): Graph[PortfolioOptimizerShape[K, V], _] = {
    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      type KM = (K, Matrix)
      type KD = (K, Double)
      type KW = (K, Weights)
      type KB = (K, Boolean)

      val symbols = (0 until size).map(_.toString) // FIXME pass actual symbols
      val sync = b.add(Synchronizer[(K, Any)]("opt", 4, 5000)(kic.pipeline.implicits.Conversions.tupleOrdering[K, Any]).named(s"Optimizer: sync streams"))
      val inputs = b.add(ZipWith[KM, KM, KM, E, PortfolioOptimizerInput[K, V]]((er, or, cov, bk) => PortfolioOptimizerInput[K, V](er._1, er._2, or._2, cov._2, bk._2)))
      val outputs = b.add(UnzipWith[PortfolioOptimizerOutput[K, V], KW, KB, E, E, E, E, E, E]((i) =>
        ((i.key, i.weights), (i.key, i.rebalanced), (i.key, i.portfolioRisk), (i.key, i.portfolioReturn), (i.key, i.benchmarkReturn), (i.key, i.alpha), (i.key, i.oneOverNRisk) , (i.key, i.oneOverNReturn))))
      val opt = b.add(new PortfolioOptimizer[K, V](symbols, riskAversion1To100, stopLossStdDevs, fromDouble))

      sync.outlets(0).as[KM] ~> inputs.in0
      sync.outlets(1).as[KM] ~> inputs.in1
      sync.outlets(2).as[KM] ~> inputs.in2
      sync.outlets(3).as[E]  ~> inputs.in3
                                inputs.out ~> opt ~> outputs.in

      new PortfolioOptimizerShape[K, V](
        sync.inlets(0).as[KM], sync.inlets(1).as[KM], sync.inlets(2).as[KM], sync.inlets(3).as[E],
        outputs.out0, outputs.out1, outputs.out2, outputs.out3, outputs.out4, outputs.out5, outputs.out6, outputs.out7
      )
    }
  }

}
