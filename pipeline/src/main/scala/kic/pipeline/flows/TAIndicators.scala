package kic.pipeline.flows

import kic.pipeline.implicits.Types._

object TAIndicators {
  def apply[K: Ordering, V: Fractional]: TAIndicators[K, V] = new TAIndicators[K, V]()
}

class TAIndicators[K: Ordering, V: Fractional] {

  // define an implicit numeric tupl which we can use to get zeros and ones and so on
  implicit val iNnum: Numeric[NumericEvent[K, V]] = (null.asInstanceOf[K], null.asInstanceOf[V])

  // do not force the api user to import all the implicits
  //implicit val iOrderng = tupleOrdering[K, V]

  // shorten (Key, Value) as E (Event or Experiment)
  type E = (K, V)

  // TODO i would like to somehow use ta-lib.org functions but I need to decide how to implement them, maybe using a code generator tool?

  /**
    * Moving average convergence divergence
    *
    */
  def macd(fastWindow: Int = 12, slowWindow: Int = 26, signalWindow: Int = 9) = ???
}
