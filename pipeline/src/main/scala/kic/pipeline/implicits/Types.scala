package kic.pipeline.implicits

import org.apache.commons.math3.linear.{Array2DRowRealMatrix, RealMatrix}

import scala.math.Ordering

/**
  * Created by kic on 29.12.16.
  */
object Types {

  implicit class Event[K, V](event: (K, V))(implicit o: Ordering[K]) extends Product2[K, V] with Ordered[Event[K, _]] {

    def maxKey(a: (K, _), b: (K, _)): K = maxKey(a._1, b._1)

    def maxKey(a: (K, _), b: Event[K, _]): K = maxKey(a._1, b._1)

    def maxKey(a: Event[K, _], b: Event[K, _]): K = maxKey(a._1, b._1)

    def maxKey(a: K, b: K): K = if (a == null) b else if (b == null) a else o.max(a, b)

    def asTupl(): Tuple2[K, V] = event

    override def compare(that: Event[K, _]): Int = o.compare(_1, that._1)

    override def _1: K = event._1

    override def _2: V = event._2

    override def canEqual(that: Any): Boolean = true

    override def toString: String =  "Event: (" + _1 + "," + _2 + ")"

  }

  /* TODO I guess we could simplify the NumericEvent to something like this:
  implicit class NumericEvent[K, V](event: (K, V))(implicit o: Ordering[K], f: Fractional[(K, V)]) extends Event[K, V](event) {
    def + (b: (K, V)):(K, V) = f.plus(event, b);
  } */

  implicit class NumericEvent[K, V](event: (K, V))(implicit o: Ordering[K], f: Fractional[V]) extends Event[K, V](event) with Numeric[NumericEvent[K, V]] with Fractional[NumericEvent[K, V]] {

    def + (b: NumericEvent[K, V]):(K, V) = plus(this, b).asTupl();

    def - (b: NumericEvent[K, V]):(K, V) = minus(this, b).asTupl();

    def * (b: NumericEvent[K, V]):(K, V) = times(this, b).asTupl();

    def / (b: NumericEvent[K, V]):(K, V) = div(this, b).asTupl();

    def / (b: Int):(K, V) = div(this, fromInt(b)).asTupl();

    def compareValueTo(that: NumericEvent[K, V]) = (maxKey(event, that), f.compare(event._2, that._2))

    def signChangeTo(that: NumericEvent[K, V]) =
      (maxKey(event, that),
        if (f.compare(event._2, f.zero) <= 0 && f.compare(that._2, f.zero) > 0)
          f.one
        else if (f.compare(event._2, f.zero) >= 0 && f.compare(that._2, f.zero) < 0)
          f.negate(f.one)
        else
          f.zero
      )

    def toDouble(): Double = toDouble(event)

    override def zero: NumericEvent[K, V] = (event._1, f.zero)

    override def one: NumericEvent[K, V] = (event._1, f.one)

    override def abs(x: NumericEvent[K, V]): NumericEvent[K, V] = super.abs(x)

    override def signum(x: NumericEvent[K, V]): Int = super.signum(x)

    override def plus(x: NumericEvent[K, V], y: NumericEvent[K, V]): NumericEvent[K, V] = (maxKey(x,y), f.plus(x._2, y._2))

    override def minus(x: NumericEvent[K, V], y: NumericEvent[K, V]): NumericEvent[K, V] = (maxKey(x,y), f.minus(x._2, y._2))

    override def times(x: NumericEvent[K, V], y: NumericEvent[K, V]): NumericEvent[K, V] = (maxKey(x,y), f.times(x._2, y._2))

    override def negate(x: NumericEvent[K, V]): NumericEvent[K, V] = (x._1, f.negate(x._2))

    override def fromInt(x: Int): NumericEvent[K, V] = (event._1, f.fromInt(x))

    override def toInt(x: NumericEvent[K, V]): Int = f.toInt(event._2)

    override def toLong(x: NumericEvent[K, V]): Long = f.toLong(event._2)

    override def toFloat(x: NumericEvent[K, V]): Float = f.toFloat(event._2)

    override def toDouble(x: NumericEvent[K, V]): Double = f.toDouble(event._2)

    override def div(x: NumericEvent[K, V], y: NumericEvent[K, V]): NumericEvent[K, V] = (maxKey(x,y), f.div(x._2, y._2))

    override def compare(x: NumericEvent[K, V], y: NumericEvent[K, V]): Int = o.compare(x._1, y._1)

  }

  implicit class SeqNumericEvent[K, V](events: Seq[(K, V)])(implicit o: Ordering[K], f: Fractional[V]) {
    import kic.pipeline.implicits.Conversions.tuplNumeric

    def to1NMatrix(): Matrix = Vec[V](AsRowVector, events).toMatrix()

    def toM1Matrix(): Matrix = Vec[V](AsColumnVector, events).toMatrix()

    def avg(): (K, V) = events.sum / events.length

    def deMean(): Seq[(K, V)] = {
      val mean = avg()
      events.map(x => (x._1, (x - mean)._2))
    }

  }

  implicit class SeqSeqNumericEvent[K, V](events: Seq[Seq[(K, V)]])(implicit o: Ordering[K], f: Fractional[V]) {

    def toColumnMatrix(): Matrix = {
      val m = new Array2DRowRealMatrix(events(0).length, events.length)

      for (i <- 0 until events(0).length; j <- 0 until events.length) {
        m.setEntry(i, j, f.toDouble(events(j)(i)._2))
      }

      m
    }

    def toRowMatrix(): Matrix = {
      val m = new Array2DRowRealMatrix(events.length, events(0).length)

      for (i <- 0 until events.length; j <- 0 until events(0).length) {
        m.setEntry(i, j, f.toDouble(events(i)(j)._2))
      }

      m
    }
  }

  implicit def convertEventCollections[K: Ordering, V](seq: Seq[(K, V)]): Seq[Event[K, V]] = seq.map(x => x: Event[K, V])

  implicit def convertNumericEventCollections[K: Ordering, V: Fractional](seq: Seq[(K, V)]): Seq[NumericEvent[K, V]] = seq.map(x => x: NumericEvent[K, V])

  implicit def convertSeqNumericToColumnVector[V: Fractional](seq: Seq[V])(implicit fractional: Fractional[V]): Matrix = {
    val mx = new Array2DRowRealMatrix(seq.length, 1)
    for (i <- 0 until seq.length) mx.setEntry(i, 0, fractional.toDouble(seq(i)))
    mx
  }

  // This way we can implement and switch to any matrix library in one single place
  // later we could rip out this part feed it back via different dependencies for different matrix implementations
  // i love this shit ...
  trait Matrix extends Fractional[Matrix] with Serializable {
    def matrixImpl[T]: T
    def + (b: Matrix): Matrix
    def - (b: Matrix): Matrix
    def * (b: Matrix): Matrix
    def *[F: Fractional] (s: F)(implicit f: Fractional[F]): Matrix
    def /[F: Fractional] (s: F)(implicit f: Fractional[F]): Matrix
    def transpose (): Matrix
    def transposeTimesItself (): Matrix
    def \ (b: Matrix): Matrix
    def m (): Int
    def n (): Int
    def getElement(i: Int, j:Int): Double
  }

  sealed trait VectorAlignment
  object AsRowVector extends VectorAlignment
  object AsColumnVector extends VectorAlignment

  sealed trait VectorData[V]
  case class Vec[V](align: VectorAlignment, data: Seq[(_, V)])(implicit fc: Fractional[V]) extends VectorData[V] {
    def getDouble(i: Int) = fc.toDouble(data(i)._2)
  }

  case class Mx[V](align: VectorAlignment, data: Seq[Seq[(_, V)]])(implicit fc: Fractional[V]) extends VectorData[V] {
    def getDouble(i: Int, j: Int) = fc.toDouble(data(j)(i)._2)
  }

  implicit class MatrixFromVectorData[V: Fractional](vec: VectorData[V]) {
    //TODO later enable BigMatrixImpl in case V is of type BigDecimal

    vec match {
      case Vec(align, data) => {

      }

      case Mx(align, data) => {

      }
    }
  }

  implicit class MatrixVector[V: Fractional](vec: Vec[V]) {
    //TODO later enable BigMatrixImpl in case V is of type BigDecimal

    private def fill(mx: RealMatrix, filler: (RealMatrix, Int, Double) => Unit): RealMatrix = {
      vec.data.indices.foreach(i => filler(mx, i, vec.getDouble(i)))
      mx
    }

    def toMatrix(): RealMatrix =
      vec.align match {
        case AsRowVector => fill(new Array2DRowRealMatrix(vec.data.length, 1), (vec, i, v) => vec.setEntry(i, 0, v))
        case AsColumnVector => fill(new Array2DRowRealMatrix(1, vec.data.length), (vec, i, v) => vec.setEntry(0, i, v))
      }

  }

  implicit class ApacheMatrix(realMatrix: RealMatrix) extends Matrix {

    override def matrixImpl[T]: T = realMatrix.asInstanceOf[T]

    override def + (b: Matrix) = ???

    override def - (b: Matrix) = ???

    override def * (b: Matrix) = realMatrix.multiply(b.matrixImpl[RealMatrix])

    override def *[F: Fractional](s: F)(implicit f: Fractional[F]) = realMatrix.scalarMultiply(f.toDouble(s))

    override def /[F: Fractional](s: F)(implicit f: Fractional[F]) = realMatrix.scalarMultiply(f.toDouble(f.div(f.one, s)))

    override def transpose() = realMatrix.transpose()

    override def transposeTimesItself() = realMatrix.transpose().multiply(realMatrix)

    override def \ (b: Matrix) = ???

    override def m = realMatrix.getRowDimension

    override def n = realMatrix.getColumnDimension


    override def getElement(i: Int, j: Int): Double = realMatrix.getEntry(i, j)

    override def div(x: Matrix, y: Matrix): Matrix = ???

    override def plus(x: Matrix, y: Matrix): Matrix = ???

    override def minus(x: Matrix, y: Matrix): Matrix = ???

    override def times(x: Matrix, y: Matrix): Matrix = ???

    override def negate(x: Matrix): Matrix = ???

    override def fromInt(x: Int): Matrix = ???

    override def toInt(x: Matrix): Int = ???

    override def toLong(x: Matrix): Long = ???

    override def toFloat(x: Matrix): Float = ???

    override def toDouble(x: Matrix): Double = ???

    override def compare(x: Matrix, y: Matrix): Int = ???

    override def toString = s"ApacheMatrix(${realMatrix})"
  }

}
