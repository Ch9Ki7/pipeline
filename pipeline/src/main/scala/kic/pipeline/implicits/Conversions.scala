package kic.pipeline.implicits

import scala.math.Ordering

/**
  * Created by kic on 01.01.17.
  */
object Conversions {

  implicit def tuplNumeric[K, V: Fractional](implicit o: Ordering[K], n: Numeric[V]): Numeric[(K, V)] = {
    def maxKey(a: (K, _), b: (K, _)) = if (a._1 == null) b._1 else if (b._1 == null) a._1 else o.max(a._1, b._1)

    new Numeric[(K, V)] {
      override def plus(x: (K, V), y: (K, V)): (K, V) = (maxKey(x, y), n.plus(x._2, y._2))

      override def minus(x: (K, V), y: (K, V)): (K, V) = (maxKey(x, y), n.minus(x._2, y._2))

      override def times(x: (K, V), y: (K, V)): (K, V) = (maxKey(x, y), n.times(x._2, y._2))

      override def negate(x: (K, V)): (K, V) = (x._1, n.negate(x._2))

      override def fromInt(x: Int): (K, V) = (null.asInstanceOf[K], n.fromInt(x))

      override def toInt(x: (K, V)): Int = n.toInt(x._2)

      override def toLong(x: (K, V)): Long =  n.toLong(x._2)

      override def toFloat(x: (K, V)): Float = n.toFloat(x._2)

      override def toDouble(x: (K, V)): Double = n.toDouble(x._2)

      override def compare(x: (K, V), y: (K, V)): Int = ???
    }
  }

  implicit def tupleOrdering[T1: Ordering, T2](implicit ord1: Ordering[T1]): Ordering[(T1, T2)] =
    new Ordering[(T1, T2)] {
      def compare(x: (T1, T2), y: (T1, T2)): Int = ord1.compare(x._1, y._1)
    }
}
