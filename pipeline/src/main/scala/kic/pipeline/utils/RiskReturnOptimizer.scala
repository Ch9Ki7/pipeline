package kic.pipeline.utils

import java.math.BigDecimal

import kic.pipeline.`lazy`.Logging
import kic.pipeline.implicits.Types.Matrix
import org.ojalgo.optimisation.{ExpressionsBasedModel, Variable}

/**
  * Created by kic on 07.01.17.
  */
case class RiskReturnOptimizer(covariances: Matrix, expectedReturnsM1: Matrix) extends Logging {
  require(covariances.m == covariances.n, "Matrix needs to be symetric")
  require(expectedReturnsM1.n == 1, "Expected returns need to be a column vector")
  private val COVARIANCES: String = "Covariances"
  private val SUBJECT: String = "x'1 = 1"
  private val EPSILON: Double = 0.0001d
  private val HALF: Double = 0.5d
  private val ZERO: Double = 0d
  private val ONE: Double = 1d


  def optimize(riskAversion0To100: Int = 50, minPosSize: Double = 0.01): Array[Double] = {
    val variables = makeModelVariables(BigDecimal.valueOf(100d - riskAversion0To100).negate)
    val weights = (0 until covariances.m + 1).map(i => if (i < covariances.m) 0d else 1d).toArray[Double]

    if (variables.isDefined) {
      val model = makeQuadrticExpression(variables.get, BigDecimal.valueOf(riskAversion0To100).pow(2))
      val optimal = model.minimise()

      if (optimal.getState.isFeasible) {
        info(s"SOLUTION: $optimal")

        (0 until weights.length - 1).foreach(i => {
          val d = optimal.get(i).doubleValue()
          if (d >= minPosSize) {
            weights(i) = d
            weights(weights.length - 1) -= d
          }
        })

      } else {
        warn(s"Optimization problem was not feasible $optimal")
      }
    } else {
      info(s"there was no valid shelf")
    }

    weights
  }

  def makeModelVariables(scaling: BigDecimal): Option[Array[Variable]] = {
    val weights = new Array[Variable](covariances.m)
    var hasShelf = false

    for (i <- 0 until expectedReturnsM1.m) {
      val eR = expectedReturnsM1.getElement(i, 0)
      weights(i) = new Variable(i.toString)
      weights(i).lower(ZERO) // we are constraining the inequalities of our variable here!

      if (eR < EPSILON) {
        weights(i).upper(ZERO) // do not invest in negaive returning positions
      } else {
        hasShelf = true
      }

      weights(i).weight(BigDecimal.valueOf(eR).multiply(scaling))
    }

    if (hasShelf) Some(weights) else None
  }

  def makeQuadrticExpression(variables: Array[Variable], scaling: BigDecimal): ExpressionsBasedModel = {
    val model = new ExpressionsBasedModel(variables)
    val quadraticExpression = model.addExpression(COVARIANCES)
    val weights = model.addExpression(SUBJECT)

    for (i <- 0 until covariances.m; j <- 0 until covariances.n) {
      quadraticExpression.setQuadraticFactor(i, j, BigDecimal.valueOf(covariances.getElement(i, j)).multiply(scaling))
    }

    for (i <- 0 until covariances.m) {
      weights.setLinearFactor(i, ONE)
    }

    weights.level(ONE) // weights should sum up to exactly 1

    model
  }
}
