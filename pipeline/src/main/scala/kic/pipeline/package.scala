package kic

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}

import scala.util.control.Exception._

/**
  * Created by kic on 13.11.16.
  */
package object pipeline {
  val decider: Supervision.Decider = {
    // case _: ArithmeticException => Supervision.Resume
    case ex =>
      ex.printStackTrace()
      Supervision.Stop
  }

  implicit val system = ActorSystem("QuickStart")
  // implicit val materializer = ActorMaterializer() (system) // expects an implicit context (ActorSystem)
  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider)) (system) // expects an implicit context (ActorSystem)
  implicit val executionContext = system.dispatcher // needed for the futures like in graph 2

  class ISODate(isoDateString: String) extends Ordered[ISODate] {
    val str = isoDateString
    val parts = isoDateString.split("\\D+")
    var year: Int = parts(0).toInt
    val month: Int = parts(1).toInt
    val day: Int = parts(2).toInt

    override def compare(that: ISODate): Int = str.compareTo(that.str)

    override def toString = isoDateString
  }

  implicit class StringNumberImprovements(val s: String) {
    def isDoubleNumber(): Boolean = (allCatch opt s.toDouble).isDefined
    def isFloatNumber(): Boolean = (allCatch opt s.toFloat).isDefined
    def isLongNumber(): Boolean = (allCatch opt s.toLong).isDefined
    def isIntNumber(): Boolean = (allCatch opt s.toInt).isDefined
    def toISODate(): ISODate = new ISODate(s)
  }
}
