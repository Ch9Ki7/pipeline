package kic.pipeline.stages

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Concat, Flow, GraphDSL, RunnableGraph, Sink, Source, Zip}
import akka.stream.{ActorMaterializer, ClosedShape}
import org.scalatest._

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import kic.pipeline.Implicits4Tests._

class TakeXThenStream$Test  extends FlatSpec {

  "1" should "by 1" in {
    val expected = Vector((2,Vector(1, 2)), (3,3), (4,4))
    val sink: Sink[(_, _), Future[Seq[(_,_)]]] = Sink.seq[(_,_)]

    val res = RunnableGraph.fromGraph(GraphDSL.create(sink) { implicit b => sink =>
      import GraphDSL.Implicits._
      println("-------------------------")

      val src = Source(List((1,1), (2,2), (3,3), (4,4)))
      val take:TakeXThenStreamShape[Int, Int] = b.add(new TakeXThenStream[Int, Int](2))
      val concat = b.add(Concat[(_, _)](2))

             take.out0 ~> concat
      src ~> take.in
             take.out1 ~> concat ~> sink

      ClosedShape
    })

    val future = res.run()
    val got = Await.result(future, 1.minutes)

    println(s"expected $expected\ngot      $got")
    assert(expected.sameElements(got) && got.sameElements(expected))
  }

}
