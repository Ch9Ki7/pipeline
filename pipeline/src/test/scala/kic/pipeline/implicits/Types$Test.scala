package kic.pipeline.implicits

import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}


/**
  * Created by kic on 29.12.16.
  */
class Types$Test extends FeatureSpec with GivenWhenThen with Matchers {
  import Types._

  feature("implicit types for quantitative tuples") {


    scenario("getting default ZERO and ONE from empty definitions") {
      Given("a function which takes any NumericEvent")
      def fTest[K: Ordering, V: Fractional](e: NumericEvent[K,V]): List[NumericEvent[K, V]] = List(
        (null.asInstanceOf[K], null.asInstanceOf[V]).zero,
        (null.asInstanceOf[K], null.asInstanceOf[V]).one
      )

      When("we pass any numeric event")
      val res = fTest((1, 1.2))

      Then("we can obain the defult values for ZERO and ONE")
      info(s"res: $res")
      assert(List((null, 0.0), (null, 1.0)) == res.map(e => e.asTupl()))
    }


    scenario("call a function expecting a more specific (yet implied) type") {

      Given("an event and a function(x: Event) ... ")
      val e = (12, "hello??")
      val eN = (12, 23.1)
      def f(e: Event[_,_]): String = s"$e"
      def fN(e: NumericEvent[_,_]): String = s"$e"
      def fX[K: Ordering, V: Fractional](e: NumericEvent[K,V]): NumericEvent[K, V] = e + e

      When("we call those functions")
      val r = f(e)
      val rN = fN(eN)
      val rX = fX(eN)

      Then("we get a valid result for the implied case")
      info(s"$r, $rN, $rX")
      assert("Event: (12,hello??)" == r)
      assert("Event: (12,23.1)" == rN)
      assert("Event: (12,46.2)" == rX.toString)
    }


    scenario("add") {

      Given("two events")
      val a1 = (null.asInstanceOf[Int], 21.0)
      val b1 = (12, 23.1)

      val a2 = (11, 21.0)
      val b2 = (12, 23.1)

      When("we add those two events")
      val sum1 = a1 + b1
      val sum2 = a2 + b2
      val sum3 = sum1 + sum2

      Then("we get the bigger (or not null) key with the sum of the values")
      info(s"$sum1, $sum2, $sum3")

      assert(sum1 === sum2)
      assert((12, 44.1) == sum1)
      assert((12, 88.2) == sum3)

    }

    scenario("tuples should be implicitly ordered by keys only") {
      import kic.pipeline.implicits.Conversions.tupleOrdering

      Given("two tupels with the same key but with different values")
      val a = (1, 1.0)
      val b = (1, 1.1)

      When("we the compare this two tuples")
      def comp[T](a: T, b: T)(implicit o: Ordering[T]): Int = o.compare(a, b)

      Then("the value needs to be 0")
      //assert(0 == comp(a, b)(tupleOrdering))
      assert(0 == comp(a, b))
    }

    scenario("collection of tuples") {
      import kic.pipeline.implicits.Conversions.tuplNumeric

      Given("a list of numeric tuples")
      val s = List((1,1.0), (2, 2.0))

      When("we use the sum function")
      val sum = s.sum

      Then("we expect a new tupl with the max key and the sum of all values")
      info(s"sum=$sum")
      assert((2,3.0) == sum)
    }
  }
}
