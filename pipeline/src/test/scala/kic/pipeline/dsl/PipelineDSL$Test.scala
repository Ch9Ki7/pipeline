package kic.pipeline.dsl

import akka.NotUsed
import akka.stream.ClosedShape
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink, Source}
import kic.pipeline.flows.Quant
import kic.pipeline.{Data, ISODate}
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}
import kic.pipeline.Implicits4Tests._

/**
  * Created by kic on 04.01.17.
  */
class PipelineDSL$Test extends FeatureSpec with GivenWhenThen with Matchers {

  feature("pipelines DSL") {
    scenario("we want to build a portfolio optimizer with our shapes") {

      val symbols = Array("AAPL", "MSFT", "GOOG", "AMZN")
      val bmSymbol = "QQQ"
      val startDate = new ISODate("2015-01-01")
      val endDate =  new ISODate("2016-01-31")
      val covarianceWindowSize = 100
      val qt = Quant[ISODate, Double]

      // type E = (ISODate, Double)



      // next we want to make a PlotSink
      val g = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
        import GraphDSL.Implicits._
        import PipelineDSL.FromSource

        val sources = symbols.map(Data.yahooData(_, "close", startDate, endDate))
        val ignore = () => Sink.ignore

        //val x: FromSource[(ISODate, Double)]  = new FromSource[(ISODate, Double)](sources)
        //val y: FromSource[Source[(ISODate, Double), _]] = sources
        sources.foo

        val x = sources ~> qt.price2Return()



        /* TODO this is how it should look like (as close as possible)

        shelf     ~> price2Return ~> macd(12, 26, 9).histogram ~> fanIntoColumnVector    ~> optimizer.in0ExpectedReturns                    // shelf is a sequence of sources* and ignore unused macd outlets
                                  ~>                                                        optimizer.in1ObservedReturns                    // automatically boradcast returns
                                  ~>                              fanIntoCovariance(100) ~> optimizer.in2Covariances                        // automatically boradcast returns
        benchmark ~> price2Return ~>                                                        optimizer.in3BenchmarkReturns                   // operate an single sources as well
                                                                                            optimizer.out2PortfolioReturn  ~> plot(line)    // ignore the other two outlets by default


        *) or just symbols maybe even better?


        */

        Source.empty ~> Sink.ignore

        // return the closed graph shape
        ClosedShape
      })

      // g.run()
    }
  }

}
