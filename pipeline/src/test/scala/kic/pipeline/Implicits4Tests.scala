package kic.pipeline

import java.util

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{RunnableGraph, Sink, Source}
import kic.pipeline.javainterfaces.Plotter
import org.scalactic.TolerantNumerics

import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.collection.JavaConverters._

/**
  * Created by kic on 28.12.16.
  */
object Implicits4Tests {

  implicit val system = ActorSystem("Akka-Unit-Test")
  implicit val materializer = ActorMaterializer() (system) // expects an implicit context (ActorSystem)
  implicit val executionContext = system.dispatcher // needed for the futures like in graph 2
  implicit val doubleEquality = TolerantNumerics.tolerantDoubleEquality(0.001) // make double compare fault tolarant

  implicit class NrTupls[K, V: Fractional](seq: Iterable[(K, V)])(implicit f: Fractional[V]) extends Iterable[(K, V)] {
    def ==== (b: Iterable[(_, _)]): Boolean = {
      val it1 = seq.iterator
      val it2 = b.seq.iterator

      while (it1.hasNext && it2.hasNext) {
        val a = it1.next()
        val b = it2.next()

        if (a._1 != b._1) {
          return false
        }

        if (f.toDouble(f.abs(f.minus(a._2, b._2.asInstanceOf[V]))) > 0.001) {
          return false
        }
      }

      return !(it1.hasNext || it2.hasNext)
    }

    override def iterator: Iterator[(K, V)] = seq.iterator
  }

  implicit class TestGraphRunner[T](g: RunnableGraph[Future[Seq[T]]]) {
    def exec(): Seq[T] = Await.result(g.run(), 20.seconds)
  }

  implicit class TestStreamRunner[T](flow: Source[T, _]) {
    def exec(): Seq[T] = Await.result(flow.runWith(sink[T]()), 20.seconds)
  }

  case class SeqPlotter() extends Plotter {
    var _ds: Option[(String, String)] = None
    val _x = mutable.ListBuffer[Any]()
    val _y = mutable.ListBuffer[Any]()

    override def onChart(chartId: String) = println(s"new chart: $chartId")

    override def onDataSet(dataSetId: String, plotType: String) = _ds = Some((dataSetId, plotType))

    override def plot(chartId: String, dataSetId: String, x: scala.Any, y: scala.Any) = {
      // println(s"plot ($x, $y)")
      _x += x
      _y += y
    }

    override def plot(chartId: String, dataSetId: String, x: scala.Any, y: scala.Any, subChart: java.util.Map[_, _]): Unit = {
      _x += Tuple2(x, y)
      _y += subChart.asScala
    }
  }

  def sink[T](): Sink[T, Future[Seq[T]]] = Sink.seq[T]

  def sinkPrint(prefix: String) = Sink.foreach[Any](x => println(s"$prefix: $x"))

  def sinkIgnore() = Sink.ignore
}
