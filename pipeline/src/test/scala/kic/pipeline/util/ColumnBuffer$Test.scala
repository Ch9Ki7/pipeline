package kic.pipeline.util

import java.util

import kic.pipeline.stages._
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

import scala.collection.immutable.Seq

/**
  * Created by kic on 10.12.16.
  */
class ColumnBuffer$Test extends FeatureSpec with GivenWhenThen with Matchers {

  class Tester[T](comp: Ordering[T], input: Seq[T]*) {
    private val skips = Array.fill[Int](input.length)(0)
    private val puller = new Puller[T](input.toList)

    // show some debug info on construction time (at "given")
    input.foreach(ip => info(s"  $ip"))

    def setSkip(inletIdx: Int, skipCount: Int): Tester[T] = {
      puller.setSkip(inletIdx, skipCount)
      this
    }

    def test(): Array[Seq[Any]] = {
      val sync = SynchronizedBuffers[T]("sync-buffer-test", input.length, puller.pull, comp)
      puller.setTargetSynchronizer(sync)

      val t = Array.fill(input.length)(new util.LinkedList[T]())
      var r = 0

      while (!sync.complete()) {
        (0 until input.length).foreach(c => {
          sync.consume(c, v => t(c).add(v))
        })

        r += 1
      }

      t.foreach(x => info(s"  $x"))

      t.map(l => l.toArray.toList)
    }
  }

  class Puller[T](in: Seq[Seq[T]]) {
    private val ports = in.map(s => s.iterator)
    private val skipsOri = Array.fill[Int](in.length)(0)
    private val skips = Array.fill[Int](in.length)(0)
    private val prt = false
    private var target: Option[SynchronizedBuffers[T]] = None

    def setTargetSynchronizer(target: SynchronizedBuffers[T]): Unit = this.target = Some(target)

    def setSkip(idx: Int, cnt: Int): Unit = {
      skipsOri(idx) = cnt
      skips(idx) = cnt
    }

    def pull(idx: Int): Unit = {
      if (prt) info(s"pull $idx ")

      // we have the opportunity to make a testcase of skipping pullers. So we can emulate a ~> sma(20) ~> which skips 20 pulls
      skips(idx) -= 1

      if (skips(idx) < 0) {
        if (ports(idx).hasNext) {
          // SynchronizedBuffers#put(idx, ports(idx).next)
          if (prt) info(s" put $idx")
          target.get.put(idx, ports(idx).next)
        } else {
          // SynchronizedBuffers#close(idx)
          target.get.close(idx)
          if (prt) info(s" closed $idx")
        }

        // reset the skips here
        skips(idx) = skipsOri(idx)
      } else {
        if (prt) info(s"skipping $idx ...")
      }
    }
  }

  def assert(synchronized: Array[Seq[Any]], expected: List[List[Int]]): Unit = {
    assert(synchronized.length == expected.length)
    synchronized.indices.foreach(i => {
      info(s"  ${synchronized(i)} == ${expected(i)}")
      assert(synchronized(i) == expected(i))
    })

    // give us some space
    info("\n")
  }


  info("Testing Column Syncronizer ... ")

  feature("sychronizing independent columns based on tupl key") {

    scenario("very basic stream with gaps and one skiping port") {

      Given(s"three short sources of time series with gapps")
      val scenario = new Tester[Int](Ordering[Int],
        List(1, 2, 3),
        List(   2, 3, 4),
        List(   2,    4)
      ).setSkip(0, 5)

      When("we synchronize the streams")
      val synchronized = scenario.test()

      Then("we have a series of synchronized outputs without gaps")
      assert(synchronized, List(
        List(   2, 3, 3),
        List(   2, 3, 4),
        List(   2, 2, 4)
      ))
    }


    scenario("simple stream with gaps") {

      Given(s"three sources of time series with gaps")
      val scenario = new Tester[Int](Ordering[Int],
        List(1, 2, 3,       6, 7),
        List(   2, 3, 4,    6   ),
        List(   2,    4, 5, 6, 7)
      )

      When("we synchronize the streams")
      val synchronized = scenario.test()

      Then("we have a series of synchronized outputs without gaps")
      assert(synchronized,List(
        List(   2, 3, 3, 3, 6, 7),
        List(   2, 3, 4, 4, 6, 6),
        List(   2, 2, 4, 5, 6, 7)
      ))
    }

    scenario("simple stream with a big gap") {

      Given(s"two sources of time series with gaps and a long front runner")
      val scenario = new Tester[Int](Ordering[Int],
        List(1, 2, 3, 4, 5, 6, 7, 8, 9),
        List(1,          5            )
      )

      When("we synchronize the streams")
      val synchronized = scenario.test()

      Then("we have a series of synchronized outputs without gaps")
      assert(synchronized, List(
        List( 1, 2, 3, 4, 5, 6, 7, 8, 9 ),
        List( 1, 1, 1, 1, 5, 5, 5, 5, 5 )
      ))
    }


    scenario("simple stream not getting any data on port 1") {

      Given(s"two sources of one never emits any data")
      val scenario = new Tester[Int](Ordering[Int],
        List(1, 2, 3, 4),
        List(          )
      )

      When("we synchronize the streams")
      val synchronized = scenario.test()

      Then("we have only empty lists")
      assert(synchronized, List(List( ), List( )))
    }


    scenario("simple stream with long front runner") {

      Given(s"three sources of time series with gaps and a long front runner")
      val scenario = new Tester[Int](Ordering[Int],
        List(1, 2, 3, 4, 5      ),
        List(               6   ),
        List(                  7)
      )

      When("we synchronize the streams")
      val synchronized = scenario.test()

      Then("we have a series of synchronized outputs without gaps")
      assert(synchronized, List(
        List( 5 ),
        List( 6 ),
        List( 7 )
      ))
    }
  }
}
