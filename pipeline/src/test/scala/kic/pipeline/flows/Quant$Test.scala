package kic.pipeline.flows

import akka.stream.ClosedShape
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source, Zip, ZipWith}
import kic.pipeline.Implicits4Tests._
import kic.pipeline.implicits.Types.Matrix
import kic.pipeline.javainterfaces.Plotter
import kic.pipeline.stages.Weights
import org.joda.time.format.ISODateTimeFormat
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

/**
  * Created by kic on 06.12.16.
  */
class Quant$Test extends FeatureSpec with GivenWhenThen with Matchers {

  type E = (Int, Double)

  val qt = Quant[Int, Double]


  feature("quant stream calculations using the implicit quant types") {

    scenario("test plotter") {
      implicit val p = SeqPlotter()

      Given(s"a source of timeseries events")
      val tsEvents = List((1, 1.0), (2, 1.2), (3, 1.1), (4, 1.05))
      val source = Source(tsEvents)

      When("we calculate the returns of this timeseries")
      val plotter = qt.plotFlow[Any]("lala")
      val flow = source.via(plotter("one", "line"))
      val got = flow.exec()

      Then("we have a series of returns")
      val tsExpectedX = List(1,2,3,4)
      val tsExpectedY = List(1.0,1.2,1.1,1.05)
      info(s"res: ${p._x}, ${p._y}\nexp: $tsExpectedX, $tsExpectedY")
      assert(("one", "line") == p._ds.get)
      assert(tsExpectedX == p._x)
      assert(tsExpectedY == p._y)

    }


    scenario("convert price to returns") {

      Given(s"a source of timeseries events")
      val tsEvents = List((1, 1.0), (2, 1.2), (3, 1.1), (4, 1.05))
      val source = Source(tsEvents)

      When("we calculate the returns of this timeseries")
      val flow = source.via(qt.price2Return()())
      val got = flow.exec()

      Then("we have a series of returns")
      val tsExpected = List((2, 0.2), (3, -0.083333), (4, -0.0454545))
      info(s"res: $got\nexp: $tsExpected")
      assert(tsExpected ==== got)

    }


    scenario("convert price to performance") {

      Given(s"a source of timeseries events")
      val tsEvents = List((1, 1.0), (2, 1.2), (3, 1.1), (4, 1.05))
      val source = Source(tsEvents)

      When("we calculate the performance explicitly via returns or via the direct method")
      val flowExplicit = source.via(qt.price2Return()()).via(qt.return2Performance()())
      val flowDirect = source.via(qt.price2Performance())

      val got = flowExplicit.exec()
      val got2 = flowDirect.exec()

      Then("we have both ways returning the same result")
      val tsExpected = List((2, 0.2), (3, 0.1), (4, 0.05))
      info(s"res: $got\n     $got2 ==\nexp: $tsExpected")
      assert(tsExpected ==== got)
      assert(got ==== got2)

    }


    scenario("calculate a simple moving average over a given timeseries") {

      Given(s"a source of timeseries events")
      val tsEvents = List((1, 1.0), (2, 1.2), (3, 1.1), (4, 1.05))
      val source = Source(tsEvents)

      When("we calculate the sma(2) and a sma(0)")
      val sma = qt.sma(2)
      val sma0 = qt.sma(0)

      val flowSma = source.via(sma())
      val flowSma0 = source.via(sma0())

      val got = flowSma.exec()
      val got0 = flowSma0.exec()

      Then("we have smoothened time series")
      val tsExpected = List((2, 1.1), (3, 1.15), (4, 1.075))
      val tsExpected0 = List((1, 0.0), (2, 0.0), (3, 0.0), (4, 0.0))

      info(s"res: $got\nexp: $tsExpected")
      assert(tsExpected ==== got)

      info(s"res: $got0\nexp: $tsExpected0")
      assert(tsExpected0 ==== got0)

    }


    scenario("test zipping plot") {
      val tsEvents = List((1, 1.0), (2, 1.2), (3, 1.1), (4, 1.05))
      implicit val p = SeqPlotter()

      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[E]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val source = b.add(Source(tsEvents))
        val bc = b.add(Broadcast[(Int, Double)](2))
        val src2 = b.add(Source.single(tsEvents.toMap[Int, Double]))
        val plot = b.add(qt.plotSinkExtra("test-extra")(p)("a", "line"))

        source ~> bc.in
                  bc.out(0) ~> sink
                  bc.out(1) ~> plot.in0
                  src2 ~> plot.in1

        ClosedShape
      })

      val got = g.exec()
      val expectedX = List((1,1.0))
      val expectedY = List(List((1,1.0), (2,1.2), (3,1.1), (4,1.05)).toMap)
      info(s"expected $expectedX, $expectedY\ngot      ${p._x}, ${p._y}")
      assert(expectedX == p._x)
      assert(expectedY == p._y)

    }

    scenario("sma difference") {
      val tsEvents = List((1, 1.0), (2, 1.2), (3, 1.1), (4, 1.05))

      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[E]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val source = b.add(Source(tsEvents))
        val bcast = b.add(Broadcast[E](2))
        val flowSma = b.add(qt.sma(2)())
        val flowSma0 = b.add(qt.sma(0)())
        val minus = b.add(qt.minus())

        source ~> bcast.in
                  bcast.out(0) ~> flowSma0  ~> minus.in0
                  bcast.out(1) ~> flowSma   ~> minus.in1
                                               minus.out ~> sink

        ClosedShape
      })

      val got = g.exec()
      val tsExpected = List((2, -1.1), (3, -1.15), (4, -1.075))
      info(s"expected $tsExpected\ngot      $got")
      assert(tsExpected ==== got)

    }


    scenario("covariance") {
      Given("two indipendent streams")
      val x = List((1, -0.2), (3,  0.1), (4,  0.05), (5,  0.05))
      val y = List((2,  0.2), (3, -0.1), (4, -0.05), (5, -0.05))

      When("we execute the covariance graph")
      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[(Int, Matrix)]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val srcX = b.add(Source(x))
        val srcY = b.add(Source(y))

        val cov = b.add(qt.fanIntoCovariance(2, 3))

        srcX ~> cov.in(0)
        srcY ~> cov.in(1)
                cov.out ~> sink

        ClosedShape
      })

      Then("we get a stream of covariances")
      val got = g.exec()
      val flat = got.map(x => List(x._2.getElement(0,0), x._2.getElement(0,1), x._2.getElement(1,0), x._2.getElement(1,1)))

      val expected = Vector(
        List(0.025833333, -0.025833333, -0.025833333, 0.025833333),
        List(0.000833333, -0.000833333, -0.000833333, 0.000833333)
      )

      info(s"expected: $expected\ngot:      $flat")

      for (i <- 0 until 2; j <- 0 until 4) {
        flat(i)(j) should be (expected(i)(j) +- 0.0001)
      }
    }


    scenario("macd") {
      val tsEvents = List((1, 1.0), (2, 2.0), (3, 3.0), (4, 4.0))

      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[E]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val source = b.add(Source(tsEvents))
        val macd = b.add(qt.macd(2, 3, 0, qt.sma)())

        source ~> macd.in
                  macd.outlets(0) ~> sink
                  macd.outlets(1) ~> sinkPrint("signal")
                  macd.outlets(2) ~> sinkPrint("histogram")

        ClosedShape
      })

      val got = g.exec()
      val expected = List((3,0.5), (4,0.5))

      info(s"expected $expected\ngot      $got")
      assert(expected ==== got)
    }

    scenario("portfolio optimizer") {
      implicit def fromDouble[V](d: Double): V = d.asInstanceOf[V]
      type KAD = (Int, Array[Double])
      type OUT = (KAD, E, E)

      val tsDax: List[E] = List((1, 10283.44043), 	(2, 10743.009766), 	(3, 10860.139648), 	(4, 10653.910156), 	(5, 10727.639648), (6, 10488.75),   	(7, 10497.769531),  (8, 10608.19043), 	(9, 10738.120117))
      val tsSmi: List[E] = List((1, 8656.299805), 	(2, 8818.099609), 	(3, 8883), 	        (4, 8739.400391), 	(5, 8705.700195),  (6, 8515.799805), 	(7, 8544.400391), 	(8, 8608.900391), 	(9, 8656.299805))
      val tsSp5: List[E] = List((1, 2012.660034), 	(2, 2043.939941), 	(3, 2063.360107), 	(4, 2078.360107), 	(5, 2056.5), 	     (6, 2060.98999), 	(7, 2064.290039), 	(8, 2038.969971), 	(9, 2021.150024))
      val rawSources: List[List[E]] = List(tsDax, tsSmi)

      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[OUT]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val cov = b.add(qt.fanIntoCovariance(rawSources.length, 6))
        val obsRet = b.add(qt.fanIntoRowVector(rawSources.length))
        val expRet = b.add(qt.fanIntoRowVector(rawSources.length))
        val bench = b.add(Source(tsSp5))
        val benchToRet = b.add(qt.price2Return()())
        val opt = b.add(qt.optimizePortfolio(rawSources.length, 50))
        val zip = b.add(ZipWith[(Int, Array[Double]), E, E, OUT]((w, ooRisk, ooRet) => (w, ooRisk, ooRet)))

        rawSources.indices.map(i => {
          val src = b.add(Source(rawSources(i)))
          val toRet = b.add(qt.price2Return()())
          val bcRet = b.add(Broadcast[E](3))
          val toPerf = b.add(qt.return2Performance()())
          val macd = b.add(qt.macd(3, 5, 0, qt.sma)())


          src ~> toRet ~> bcRet.in
                          bcRet.out(0) ~> cov.in(i)
                          bcRet.out(1) ~> obsRet.in(i)
                          bcRet.out(2) ~> toPerf ~> macd.in
                                                    macd.outlets(0) ~> expRet.in(i)
                                                    macd.outlets(1) ~> Sink.ignore
                                                    macd.outlets(2) ~> Sink.ignore
        })

        expRet.out               ~> opt.in0ExpectedReturns
        obsRet.out               ~> opt.in1ObservedReturns
        cov.out                  ~> opt.in2Covariances
        bench      ~> benchToRet ~> opt.in3BenchmarkReturns
                                    opt.out0Weights           ~> Flow[(Int, Weights)].map({case (k, w) => (k, w.weights())}) ~> zip.in0
                                    opt.out1Rebalanced        ~> Sink.ignore
                                    opt.out2PortfolioRisk     ~> Sink.ignore
                                    opt.out3PortfolioReturn   ~> Sink.ignore
                                    opt.out4BenchmarkReturn   ~> Sink.ignore
                                    opt.out5Alpha             ~> Sink.ignore
                                    opt.out6OneOverNRisk      ~> zip.in1
                                    opt.out7OneOverNReturn    ~> zip.in2
                                                                 zip.out ~> sink

        ClosedShape
      })

      //val expectedWeights  = Vector((7, Array(0.0, 0.0, 1.0)), (8, Array(0.0, 0.0, 1.0)), (9, Array(1.0, 0.0, 0.0)))
      val expectedWeights  = Vector(Array(0.0, 0.0, 1.0).toSeq, Array(0.0, 0.0, 1.0).toSeq, Array(1.0, 0.0, 0.0).toSeq)
      val expected1NRisk   = Vector((7, 0.000377071), (8, 0.000182737), (9, 0.000181516))
      val expected1NReturn = Vector((7, 0.002109228), (8, 0.009033657), (9, 0.008876958))

      val got = g.exec()
      val gotWeights = got.map(_._1._2.toSeq)
      val gotRisk = got.map(_._2)
      val gotReturn = got.map(_._3)

      info("\nWeights")
      info(s"expected: ${expectedWeights.mkString(",")}\ngot:      ${gotWeights.mkString(",")}")
      assert(expectedWeights == gotWeights)

      info("\nRisk")
      info(s"expected: $expected1NRisk\ngot:      $gotRisk")
      assert(expected1NRisk ==== gotRisk)

      info("\nReturn")
      info(s"expected: $expected1NReturn\ngot:      $gotReturn")
      assert(expected1NReturn ==== gotReturn)
    }
  }

}
