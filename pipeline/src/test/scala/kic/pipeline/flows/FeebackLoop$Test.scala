package kic.pipeline.flows

import akka.stream.ClosedShape
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink, Source, ZipWith}
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}
import scala.concurrent.duration.DurationInt
import scala.concurrent.Await
import GraphDSL.Implicits._
import kic.pipeline.Implicits4Tests._
/**
  * Created by kic on 28.12.16.
  */
class FeebackLoop$Test extends FeatureSpec with GivenWhenThen with Matchers {


  feature("feedback loop") {
    scenario("testing simple accumulating feedback loop") {

      Given("a list of data and an accumulator requiring a feedback loop")
      val src = Source(List(1,2,3,4,5))
      val sink = Sink.seq[Int]
      val graph = RunnableGraph.fromGraph(GraphDSL.create(sink) { implicit b => sink =>
        src ~> FeedbackLoop(ZipWith[Int, Int, Int]((a, b) => a + b), 0) ~> sink
        ClosedShape
      })

      When("we execute the graph")
      val future = graph.run()
      val got = Await.result(future, 1.minutes)


      Then("we get accumulated data")
      info(s"$got")
      assert(List(1, 3, 6, 10, 15) == got)
    }
  }
}
