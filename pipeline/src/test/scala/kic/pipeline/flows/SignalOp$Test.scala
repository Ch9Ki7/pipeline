package kic.pipeline.flows

import akka.stream.ClosedShape
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Source, Zip, ZipWith}
import kantan.csv
import kic.pipeline.Implicits4Tests
import kic.pipeline.Implicits4Tests._
import org.joda.time.LocalDate
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

/**
  * Created by kic on 30.12.16.
  */
class SignalOp$Test extends FeatureSpec with GivenWhenThen with Matchers {

  import kic.pipeline.implicits.Types._
  import kic.pipeline.implicits.Conversions.tupleOrdering

  type E = (Int, Double)

  type II = (Int, Int)

  def makeSimpleSyncGraph(odd: Source[E, _], even: Source[E, _]) = {
    val keysOnlyZip = () => ZipWith[E, E, II]((a, b) => (a._1, b._1))

    RunnableGraph.fromGraph(GraphDSL.create(sink[II]()) { implicit b => sink =>
      import GraphDSL.Implicits._

      val s = b.add(Synchronizer[E]("sync-test", 2))
      val zipper = b.add(keysOnlyZip())

      odd  ~> s.inlets(0) ; s.outlets(0) ~> zipper.in0
      even ~> s.inlets(1) ; s.outlets(1) ~> zipper.in1
      zipper.out ~> sink

      ClosedShape
    })
  }

  feature("Signal feature synchronize") {

    scenario("syncronize from flow") {

      Given("two sources of tupl data")
      val odd = Source(List(  (1,1.0), (3,3.0),        (5,5.0),          (8,8.0) ))
      val even = Source(List( (1,1.1),        (4,4.1), (5,5.1), (6,6.1), (8,8.1) ))

      When("we synchronize those")
      val got = makeSimpleSyncGraph(odd, even).exec()

      Then("we get perfectly synchronized keys")
      val expected = Vector((1,1), (3,1), (3,4), (5,5), (5,6), (8,8))
      info(s"expected $expected\ngot      $got")
      assert(expected == got)
    }


    scenario("syncronize from flow with only one value") {

      Given("two sources of single tupl data")
      val odd = Source(List(  (1,1.0)))
      val even = Source(List( (1,1.1)))

      When("we synchronize those")
      val got = makeSimpleSyncGraph(odd, even).exec()

      Then("we get perfectly synchronized keys")
      val expected = Vector((1,1))
      info(s"expected $expected\ngot      $got")
      assert(expected == got)
    }


    scenario("syncronize from flow with exactly two values") {

      Given("two sources of single tupl data")
      val odd = Source(List(  (1,1.0), (2, 1.0)))
      val even = Source(List( (1,1.1), (2, 1.1)))

      When("we synchronize those")
      val got = makeSimpleSyncGraph(odd, even).exec()

      Then("we get perfectly synchronized keys")
      val expected = Vector((1,1), (2,2))
      info(s"expected $expected\ngot      $got")
      assert(expected == got)
    }


    scenario("syncronize from flows where one flow never emits data") {

      Given("two sources of tupl data")
      val odd = Source(List(  (1,1.0), (3,3.0),        (5,5.0),          (8,8.0) ))
      val even = Source(List())

      When("we synchronize those")
      val got = makeSimpleSyncGraph(odd, even).exec()

      Then("we get and empty result but the stages completes fine")
      val expected = Vector()
      info(s"expected $expected\ngot      $got")
      assert(expected == got)
    }


    scenario("syncronize very unbalanced streams") {
      Given("two very unbalanced stream")
      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[II]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val long = b.add(Source(1 until 100000))
        val short = b.add(Source(80000 until 100000))
        val s = b.add(Synchronizer[Int]("sync-test", 2))
        val zipper = b.add(Zip[Int, Int])

        long  ~> s.inlets(0) ; s.outlets(0) ~> zipper.in0
        short ~> s.inlets(1) ; s.outlets(1) ~> zipper.in1
        zipper.out ~> sink

        ClosedShape
      })

      When("we synchronize this streams")
      val got = g.exec()

      Then("we should not get a stackoverflow exception")
      info(s"$got")
    }

    scenario("synchronize real life streams") {
      import kantan.csv.ops._                                 // Enriches types with useful methods.
      import kantan.csv.joda.time._                           // support for joda time
      import com.github.nscala_time.time.OrderingImplicits._  // implicit ordering for joda date / time classes

      type Bar = (LocalDate, Double, Double, Double, Double, Long, Double)
      type DDD = (LocalDate, LocalDate, LocalDate)

      implicit def barOrdering[T1: Ordering, T2, T3, T4, T5, T6, T7](implicit ord1: Ordering[T1]): Ordering[(T1, T2, T3, T4, T5, T6, T7)] =
        new Ordering[(T1, T2, T3, T4, T5, T6, T7)] {
          def compare(x: (T1, T2, T3, T4, T5, T6, T7), y: (T1, T2, T3, T4, T5, T6, T7)): Int = ord1.compare(x._1, y._1)
        }

      Given("Three Streams of CSV data")
      val smiCsv = this.getClass.getResourceAsStream("/data/csv/smi.csv").asCsvReader[Bar](',', true)
      val smiSrc = Source.fromIterator(() => smiCsv.toStream.reverseIterator)

      val sp500Csv = this.getClass.getResourceAsStream("/data/csv/sp500.csv").asCsvReader[Bar](',', true)
      val sp500Src = Source.fromIterator(() => sp500Csv.toStream.reverseIterator)

      val daxCsv = this.getClass.getResourceAsStream("/data/csv/dax.csv").asCsvReader[Bar](',', true)
      val daxSrc = Source.fromIterator(() => daxCsv.toStream.reverseIterator)

      When("we synchronize this streams")
      val g = RunnableGraph.fromGraph(GraphDSL.create(Implicits4Tests.sink[DDD]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val sync=  b.add(Synchronizer[Bar]("csv-test", 3)(barOrdering))

        smiSrc   ~> Flow[csv.ReadResult[Bar]].map(rr => rr.get) ~> sync.inlets(0)
        sp500Src ~> Flow[csv.ReadResult[Bar]].map(rr => rr.get) ~> sync.inlets(1)
        daxSrc   ~> Flow[csv.ReadResult[Bar]].map(rr => rr.get) ~> sync.inlets(2)

        val zip = b.add(ZipWith[Bar, Bar, Bar, DDD]((a1, a2, a3) => (a1._1, a2._1, a3._1)))

        sync.outlets(0) ~> zip.in0
        sync.outlets(1) ~> zip.in1
        sync.outlets(2) ~> zip.in2

        zip.out ~> sink

        ClosedShape
      })

      val got = g.exec()

      Then("we get prefectly alligned local dates")
      val expected = this.getClass.getResourceAsStream("/data/csv/synced-dates.csv").readCsv[List, DDD](',', false).map(rr => rr.get)
      info(s"expected   $expected\ngot      $got")
      assert(expected.sameElements(got) && got.sameElements(expected))
    }


    scenario("we have a long running moving average and therefore force the buffer to fill over its capacity (which is wat we want)") {
      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[II]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val src = b.add(Source(1 until 1000))
        val bcast = b.add(Broadcast[Int](2))
        val slide = b.add(Flow[Int].sliding(500, 1).map(seq => seq(seq.length -1)))
        //val slide = b.add(Flow[Int].sliding(998, 1).map(seq => seq(seq.length -1)))
        val sync = b.add(Synchronizer[Int]("test need of overflow", 2, 50)) // buffer size must be smaller than the sliding window!
        val zip = b.add(Zip[Int, Int]())

        src ~> bcast.in
               bcast.out(0) ~> slide ~> sync.inlets(0)
               bcast.out(1)          ~> sync.inlets(1)
                                        sync.outlets(0) ~> zip.in0
                                        sync.outlets(1) ~> zip.in1
                                                           zip.out ~> sink

        ClosedShape
      })

      val got = g.exec()
      val expected = (500 until 1000).map(x => (x, x))
      info(s"expected $expected\ngot      $got")
      assert(expected == got)
    }
  }


  feature("execute mathematical operations on signals") {
    scenario("plus") {
      val odd = Source(List(  (1,1.0), (3,3.0),        (5,5.0),          (8,8.0) ))
      val even = Source(List( (1,1.0),        (4,4.0), (5,5.0), (6,6.0), (8,8.0) ))

      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[E]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val plus = b.add(SignalOp[E]((a, b) => a + b))

        odd  ~> plus.in0
        even ~> plus.in1
                plus.out ~> sink

        ClosedShape
      })

      val got = g.exec()
      val expected = List((1,2.0), (3,4.0), (4,7.0), (5,10.0), (6,11.0), (8,16.0))
      info(s"expected $expected\ngot      $got")
      assert(expected ==== got)
    }


    scenario("minus") {
      val odd =  Source(List( (1,1.0)))
      val even = Source(List( (1,2.0)))

      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[E]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val minus = b.add(SignalOp[E]((a, b) => a - b))

        odd  ~> minus.in0
        even ~> minus.in1
        minus.out ~> sink

        ClosedShape
      })

      val got = g.exec()
      val expected = List((1,-1.0))
      info(s"expected $expected\ngot      $got")
      assert(expected ==== got)
    }
  }



}
