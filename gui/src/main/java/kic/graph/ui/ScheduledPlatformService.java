package kic.graph.ui;

import javafx.application.Platform;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;

/**
 * Created by kindler on 21/01/2017.
 */
public class ScheduledPlatformService extends ScheduledService<Void> {
    private final Runnable runnable;

    public ScheduledPlatformService(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Platform.runLater(runnable);
                return null;
            };
        };
    }
}
