package kic.graph.ui;

import kic.pipeline.javainterfaces.JavaPlotter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by kic on 21.01.17.
 */
public abstract class AbstractPlotQueue<T> implements JavaPlotter {
    private final LinkedList<T> q = new LinkedList<>();
    private final Object lock = new Object();
    private final int capacity;

    abstract void clear();
    abstract void plotEvent(T event);
    abstract T makeEvent(String chartId, String dataSetId, Object x, Object y, Map<?,?> extraData);

    public AbstractPlotQueue(int capacity) {
        this.capacity = capacity;
    }

    @Override
    final public void plot(String chartId, String dataSetId, Object x, Object y) {
        plot(chartId, dataSetId, x, y, null);
    }

    @Override
    final public void plot(String chartId, String dataSetId, Object x, Object y, Map subChart) {
        put(makeEvent(chartId, dataSetId, x, y, subChart == null || subChart.isEmpty() ? null : subChart));
    }

    final public void put(T event) {
        while (q.size() >= capacity) {
            LockSupport.parkNanos(1000);
        }

        synchronized (lock) {
            q.add(event);
        }
    }

    final public List<T> pollAll() {
        synchronized (lock) {
            List res = new ArrayList(q);
            q.clear();

            return res;
        }
    }

    final public Runnable poller() {
        return () -> {
            List<T> events = pollAll();

            for (T e : events) {
                plotEvent(e);
            }
        };
    }
}
