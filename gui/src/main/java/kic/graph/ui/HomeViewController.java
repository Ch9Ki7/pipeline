package kic.graph.ui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeViewController implements Initializable {
    public static HomeViewController INSTANCE = null;

    @FXML private TextField shelfSymbolsTF;
    @FXML private WebView webview;
    @FXML private TextField benchmarkTF;

    private StringProperty selfSymbols = new SimpleStringProperty("AAPL, MSFT");
    private StringProperty benchmarkSymbol = new SimpleStringProperty("QQQ");

    @FXML
    private LineChart<String, Double> lineChart;

    private final ObservableList<XYChart.Series<String, Double>> seriesWrapper = FXCollections.observableArrayList();
    private final AbstractPlotQueue fxPlotter = new FXPlotter(seriesWrapper, 50);
    private AbstractPlotQueue jsPlotter = DummyPlotter.INSTANCE;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            // load webview site
            webview.getEngine().load(this.getClass().getClassLoader().getResource("ui/html/optimizer.html").toExternalForm());
            jsPlotter = new ChartJSPlotter(webview.getEngine(), "main", 50);

            // attach series wrapper to chart
            lineChart.setData(seriesWrapper);

            // single bind UI -> property
            benchmarkTF.textProperty().bind(benchmarkSymbol);

            // two way binding UI <-> property
            shelfSymbolsTF.textProperty().bindBidirectional(selfSymbols);

            // chart poller
            ScheduledService<Void> fxChartPoller = new ScheduledPlatformService(fxPlotter.poller());
            fxChartPoller.setPeriod(Duration.millis(300));
            fxChartPoller.start();

            // new chart poller for html implementation
            ScheduledService<Void> jsChartPoller = new ScheduledPlatformService(jsPlotter.poller());
            jsChartPoller.setPeriod(Duration.millis(300));
            jsChartPoller.start();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            INSTANCE = this;
        }
    }

    @FXML
    protected void go(ActionEvent event) {
        fxPlotter.clear();
        jsPlotter.clear();
        //kic.pipeline.Data.optimizeGraph(fxPlotter, benchmarkSymbol.get(), selfSymbols.get().split("\\s*,\\s*"));
        // TODO move to this one
        kic.pipeline.Data.optimizeGraph(jsPlotter, benchmarkSymbol.get(), selfSymbols.get().split("\\s*,\\s*"));
    }

    @FXML
    protected void exit(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    protected void enter(MouseEvent event) {

    }

    @FXML
    public void loadPrices(ActionEvent actionEvent) {
        fxPlotter.clear();
        jsPlotter.clear();
        //kic.pipeline.Data.plotPricesGraph(fxPlotter, benchmarkSymbol.get(), selfSymbols.get().split("\\s*,\\s*"));
        kic.pipeline.Data.plotPricesGraph(jsPlotter, benchmarkSymbol.get(), selfSymbols.get().split("\\s*,\\s*"));
    }
}