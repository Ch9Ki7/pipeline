package kic.graph.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by kindler on 15/11/2016.
 */
public class Home extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();

        Parent root = FXMLLoader.load(getClass().getResource("HomeView.fxml"));

        Scene scene = new Scene(root, visualBounds.getWidth(), visualBounds.getHeight());
        scene.getStylesheets().add(Home.class.getResource("home.css").toExternalForm());

        stage.setScene(scene);
        stage.show();
    }

}
