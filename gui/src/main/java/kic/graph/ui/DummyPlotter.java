package kic.graph.ui;

import java.util.Map;

/**
 * Created by kindler on 23/01/2017.
 */
public class DummyPlotter extends AbstractPlotQueue {
    public static final DummyPlotter INSTANCE = new DummyPlotter(0);

    public DummyPlotter(int capacity) {
        super(capacity);
    }

    @Override
    public void onChart(String chartId) {

    }

    @Override
    public void onDataSet(String dataSetId, String type) {

    }

    @Override
    void clear() {

    }

    @Override
    void plotEvent(Object event) {

    }

    @Override
    Object makeEvent(String chartId, String dataSetId, Object x, Object y, Map extraData) {
        return null;
    }
}
