package kic.graph.ui;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import javafx.scene.web.WebEngine;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by kic on 21.01.17.
 */
public class ChartJSPlotter extends AbstractPlotQueue<String> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private static class Message {
        @JsonProperty public final String target, type, dset;

        public Message(String target, String type, String dset) {
            this.target = target;
            this.type = type;
            this.dset = dset;
        }
    }

    private static class DataPointEvent extends Message {
        @JsonProperty public final DataPoint data;

        public DataPointEvent(String target, String dset, DataPoint point) {
            super(target, "addXY", dset);
            this.data = point;
        }
    }

    private static class DataPoint {
        @JsonProperty public final Object x, y;
        @JsonProperty public final List<DataPoint> ext;

        public DataPoint(Object x, Object y, List<DataPoint> ext) {
            this.x = x;
            this.y = y;
            this.ext = ext;
        }
    }


    private static class DataSetEvent extends Message {
        @JsonProperty public final DataSet data;

        public DataSetEvent(String target, String dset, DataSet dataset) {
            super(target, "initDS", dset);
            this.data = dataset;
        }
    }

    private static class DataSet {
        @JsonProperty public final String label, plotType;

        public DataSet(String label, String plotType) {
            this.label = label;
            this.plotType = plotType;
        }
    }

    private static final ObjectMapper jsonMapper = new ObjectMapper();
    static {jsonMapper.registerModule(new JodaModule());}

    private static final String initDataset(String chartId, String chartType, String seriesId, String seriesLabel) {
        try {
            return jsonMapper.writeValueAsString(new DataSetEvent(chartId, seriesId, new DataSet(seriesLabel, chartType)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private static final String getScript(String msgJson) {
        return "parent.postMessage(" + msgJson + ", \"*\")";
    }

    private final boolean fireBug = false;
    private final WebEngine webEngine;
    private final String chartId;

    public ChartJSPlotter(WebEngine webEngine, String chartId, int capacity) {
        super(capacity);
        this.webEngine = webEngine;
        this.chartId = chartId;

        // Note here we could check the webview's content if there is a chart with this ID and throw an exception otherwise

        // enable debugging
        if (fireBug) {
            webEngine.documentProperty().addListener((prop, oldDoc, newDoc) -> webEngine.executeScript("if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened,overrideConsole=true');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened,overrideConsole=true');}"));
        }
    }

    @Override
    public void onChart(String chartId) {

    }

    @Override
    void clear() {
        // FIXME implement clear message
    }

    @Override
    public void onDataSet(String dataSetId, String type) {
        System.out.println(getScript(initDataset(chartId, type, dataSetId, dataSetId)) + "\t//XX");
        webEngine.executeScript(getScript(initDataset(chartId, type, dataSetId, dataSetId)));
    }

    @Override
    String makeEvent(String chartId, String dataSetId, Object x, Object y, Map<?, ?> extraData) {
        try {
            List<DataPoint> extra = extraData != null
                    ? extraData.entrySet().stream().map(e -> new DataPoint(e.getKey(), e.getValue(), null)).collect(Collectors.toList())
                    : null;

            DataPointEvent dataPointEvent = new DataPointEvent(chartId, dataSetId, new DataPoint(x.toString(), y, extra));
            String eventJson = jsonMapper.writeValueAsString(dataPointEvent);
            System.out.println(getScript(eventJson) + "\t//XX");
            return eventJson;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    void plotEvent(String event) {
        // System.out.println("XX\t" + getScript(event));
        webEngine.executeScript(getScript(event));
    }
}
