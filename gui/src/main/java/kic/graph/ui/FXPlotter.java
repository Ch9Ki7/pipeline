package kic.graph.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by kindler on 21/01/2017.
 */
public class FXPlotter extends AbstractPlotQueue<FXPlotter.Event> {

    public static final class Event {
        public final String seriesId;
        public final Comparable x;
        public final Number y;

        public Event(String seriesId, Comparable x, Number y) {
            this.seriesId = seriesId;
            this.x = x;
            this.y = y;
        }
    }

    private final ObservableList<XYChart.Series<String, Double>> chart;
    private final Map<String,Integer> seriesIndex = new HashMap<>();

    public FXPlotter(ObservableList<XYChart.Series<String, Double>> chart, int capacity) {
        super(capacity);
        this.chart = chart;
    }

    @Override
    public void clear() {
        Iterator<XYChart.Series<String, Double>> it = chart.iterator();

        while (it.hasNext()) {
            XYChart.Series<String, Double> series = it.next();
            series.getData().clear();
            it.remove();
        }

        seriesIndex.clear();
    }

    @Override
    public void onChart(String chartId) {

    }

    @Override
    public void onDataSet(String dataSetId, String type) {
        seriesIndex.computeIfAbsent(dataSetId, sid -> {
            ObservableList<XYChart.Data<String, Double>> data = FXCollections.observableArrayList();
            XYChart.Series<String, Double> series = new XYChart.Series();
            series.setName(sid);
            series.setData(data);
            chart.add(series);

            return chart.size() - 1;
        });
    }

    @Override
    Event makeEvent(String chartId, String dataSetId, Object x, Object y, Map extraData) {
        return new Event(dataSetId, x.toString(), (Number) y);
    }

    @Override
    void plotEvent(Event e) {
        chart.get(seriesIndex.get(e.seriesId)).getData().add(new XYChart.Data(e.x, e.y));
    }
}
