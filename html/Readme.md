## Using ChartJS conveniently not only from scalajs
 
First of all the [ChartJS API](http://www.chartjs.org/docs/) is huge nd converting everything into js.native
might be a big monkey job. Stumbling across this [blog post](https://www.yearofmoo.com/2015/04/cross-browser-custom-css-properties.html)
all the styling attributes in ChartJS should actually be css. 
  
So first of all we have a gradle task `preProcessCss` which compiles alls *.c.css to proper *.css files following 
the rules in the mentioned blog post.

Secondly the original implementation of custom props is lacking of inheritance which is somewhat crucial for css. 
So using a the [cssutilities](http://www.brothercake.com/site/resources/scripts/cssutilities/) from brothercake 
enabled the full inheritance.

Finally we wrap everything together in a jQuery plugin. The plugin looks for all canvas with the class chartjs 
and automatically initalizes a chart. The chart object can be obtained from jQuery like `$("#my-chart-id").data("chart")`


## Using c.css to style the charts

You almost can copy/paste all the object propeties from the docs. Since we do css now you need to follow this two 
rules:
 * you need to prefix all chartjs properties by --
 * poperties are - separated instead of . ie. --type: line
 * whenever arrays are used just provide the index as part of the property like: --data-datasets-0-label: Some Label
 * css does not support camel case. To work around this limitation you need to underscore all camel cases like so: 
   --data-datasets-1-border_color: #0000FF;
      
Following this rules you can stl anything upfront for all datasets and options. Distict different scales, types colors
Just by introducing different classes.

NOTE: currently it is not possible to change charts by changing the class via some javascript. This bight be done later
if needed.

## Adding data to the charts

Having now the chart object as data property in jQuery enables us to easily add data from scalajs by using th jQuery
facade. We just need to implement the fields in a js.native trait as we need them - i.e. 
`jquery("#some-id").data("chart").data.datasets.asInstanceOf[js.Array[Dataset]]`

Also for the use with or without scalajs there is also a window.postMessage handler attached. You can just send 
messages to the chart window to add,remove or update data. 

#### Prerequisites to use window message handler:
 * make sure the chart canvas has an id since this is
 * data type css propety for xy or only y data - TODO be precise here!
 

#### Messaging to the chart 
The follwoing messages are supported to be passed via `parent.postMessage({...}, "*")`:
 * `{...}`
 
 