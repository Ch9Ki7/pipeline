package kic.chart

import scala.scalajs.js
import scala.scalajs.js.{Any, JSApp, JSON, UndefOr}
import org.scalajs.dom
import org.scalajs.dom.MessageEvent
import org.scalajs.jquery.jQuery

import scala.scalajs.js.annotation._

@js.native
trait Event extends js.Object {
  val t: js.UndefOr[String] = js.native                   // message type
  val c: js.UndefOr[String] = js.native                   // target chart id
}

@js.native
trait XY extends js.Object {
  val x:Any = js.native
  val y:Any = js.native
}

@js.native
trait XYDataSet extends js.Object {
  val label: String = js.native
  val data: js.Array[XY] = js.native
}

@js.native
trait DataEvent extends js.Object {
  val c: String = js.native                   // chart
  val s: String = js.native                   // dataSet
  val d: XY = js.native                       // xy data
  val e: js.UndefOr[js.Dictionary[js.Any]] = js.native // Extra data
}

@js.native
trait UpdateSeriesEvent extends js.Object {
  val c: String = js.native                   // chart
  val s: String = js.native                   // dataSet
  val d: DataSet = js.native                  // xy data
}


@js.native
trait DataSetEvent extends js.Object {
  val c: String = js.native                   // chart
  val ct: String = js.native                  // chart type: line, bar, etc ...
  val id: String = js.native                  // dataSetId
  val l: String = js.native                   // dataSetLabel
}


@js.native
trait DataSet extends js.Object {
  var id: js.UndefOr[String] = js.native
  var label: js.UndefOr[String] = js.native
  var `type`: js.UndefOr[String] = js.native
  var data: js.UndefOr[js.Array[js.Any]] = js.native
  var ext: js.UndefOr[DataSet] = js.native
  var dataType: js.UndefOr[String] = js.native
}

@js.native
trait Data extends js.Object {
  val datasets: js.Array[DataSet] = js.native
  val dsIndex: js.Dictionary[DataSet] = js.native
  val throttle: js.UndefOr[Int] = js.native
  var labels: js.UndefOr[js.Array[js.Any]] = js.native
}

@js.native
trait ChartJS extends js.Object {
  val data: Data = js.native
  def update(): Function0[Unit] = js.native
  var updateThrottled: js.UndefOr[Function0[Unit]] = js.native
}

@JSExport
object ChartApi extends JSApp {
  val console = dom.window.console

  def throttle(callback: Function0[Unit], waitMs: Int): Function0[Unit] = {
    var timeout: Int = -1
    var previous = 0L

    var later: Function0[Unit] = () => {
      previous = js.Date.now().toLong
      timeout = -1
      callback()
    }

    return () => {
      var now = js.Date.now().toLong
      if (previous <= 0) previous = now
      var remaining = waitMs - (now - previous)

      if (remaining <= 0 || remaining > waitMs.toLong) {
        if (timeout > 0) {
          dom.window.clearTimeout(timeout)
          timeout = -1
        }

        previous = now
        callback()
      } else if (timeout < 0) {
        timeout = dom.window.setTimeout(later, remaining);
      }
    }
  }



  def initDataSet(chart: ChartJS, dataSetEvent: DataSetEvent): Unit = {
    // find the id / label in the datasets if not found add a new one else update
    var ds: Option[DataSet] = chart.data.datasets.find(ds => ds.id.isDefined && ds.id == dataSetEvent.id)
    if (!ds.isDefined) ds = chart.data.datasets.find(ds => !ds.id.isDefined)

    // if still not found create a new one
    if (!ds.isDefined) {
      chart.data.datasets.push(js.Dynamic.literal().asInstanceOf[DataSet])
      ds = Some(chart.data.datasets.last)
    }

    // update the label, type etc. and clear the data array
    ds.get.id = dataSetEvent.id
    ds.get.label = dataSetEvent.l
    ds.get.`type` = dataSetEvent.ct
    ds.get.data = js.Array[js.Any]()

    // update the index for fast access
    chart.data.dsIndex.update(dataSetEvent.id, ds.get)

    // finally update the chart
    chart.update()
  }

  def addData(chart: ChartJS, dataEvent: DataEvent): Unit = {
    val ds = chart.data.dsIndex.get(dataEvent.s)

    if (ds.isDefined) {
      val data = ds.get.data.get

      // FIXME here we need to distinct between XYData and L(abel)YData
      if (ds.get.dataType.isDefined && ds.get.dataType.get == "LY") {
        console.log("label y data !! ")
        if (chart.data.labels.isDefined) {
          chart.data.labels.get.push(dataEvent.d.x.asInstanceOf[js.Any])
        } else {
          chart.data.labels = js.Array[js.Any](dataEvent.d.x.asInstanceOf[js.Any])
        }

        data.push(dataEvent.d.y.asInstanceOf[js.Any])
      } else {
        data.push(dataEvent.d)
      }

      if (dataEvent.e.isDefined) {
        // add extra data to somewhere
        if (!ds.get.ext.isDefined) ds.get.ext = js.Dynamic.literal(data = js.Array[js.Any]()).asInstanceOf[DataSet]
        val e = dataEvent.e.get
        val ed = ds.get.ext.get.data.get
        console.log("add extra", e)
        e.foreach({case (x, y) => {ed.push(js.Dynamic.literal(x=x, y=y).asInstanceOf[XY])}})
      }

      // throttled update function
      if (chart.updateThrottled.isDefined) {
        (chart.updateThrottled.get)()
      }
    } else {
      console.log(dataEvent.s, "not found in", chart.data.datasets)
    }
  }

  def updateSeries(chart: ChartJS, newData: UpdateSeriesEvent): Unit = {

    val dataset = chart.data.dsIndex.get(newData.s)
    if (!dataset.isDefined) {
      console.log("uupdate udedf")
      // call init dataset
      initDataSet(chart, js.Dynamic.literal(
        c = newData.c,
        ct = "doughnut",
        id = newData.s,
        l  = null
      ).asInstanceOf[DataSetEvent])
    }

    if (dataset.isDefined) {
      if (dataset.get.dataType.isDefined && dataset.get.dataType.get == "LY") {
        console.log("UPDATE label y data !! ")
        dataset.get.data = newData.d.data.get.map(xy => {xy.asInstanceOf[XY].y}).asInstanceOf[js.Array[js.Any]]
        chart.data.labels = newData.d.data.get.map(xy => {xy.asInstanceOf[XY].x}).asInstanceOf[js.Array[js.Any]]
      } else {
        jQuery.extend(dataset.get, newData.d)
        console.log("uupdate", dataset.get)
      }
      chart.update()
    } else {
      console.error("dataset still undefined")
    }
  }

  def receiveMessage(event: Event): Unit = {

    if (event.t.isDefined && event.c.isDefined) {
      var selector = "#" + event.c.get.replaceAll("#", "")
      val element = jQuery(selector)

      if (element.length <= 0) {
        console.log("element not found!", selector, element)
        return
      }

      val chartNative = element.data("chart")
      if (js.isUndefined(chartNative) || chartNative == null) {
        console.log("element does not have chart object", element.data())
        return
      }

      // FIXME we want to replace or add a chart.updateThrottled where we update (max 4 times a sec = every 250ms)
      // chart.updateThrottled = throttle(chart.update(), 250)
      val chart = chartNative.asInstanceOf[ChartJS]
      if (!chart.updateThrottled.isDefined) chart.updateThrottled = throttle(() => {chart.update()}, chart.data.throttle.getOrElse(250))

      event.t.get match {
        case "initDS"       => initDataSet(chart, event.asInstanceOf[DataSetEvent])
        case "addXY"        => addData(chart, event.asInstanceOf[DataEvent])
        case "updateSeries" => updateSeries(chart, event.asInstanceOf[UpdateSeriesEvent])
        case "update"       => chart.update()
        case _              => console.log("unknown event type " + event.t, event)
      }
    }
  }

  @JSExport
  def main(): Unit = {
    // just add a window event handler
    dom.window.addEventListener("message", (event: MessageEvent) => receiveMessage(event.data.asInstanceOf[Event]), false)
  }
}