(function ($) {
    var onHoverCallBack = kic.chart.Callbacks ? function(e){kic.chart.Callbacks().onHoverSendMessage(e)} : function(e) {}

    /**
     * if we use  $.extend(true, {},{a:1, b:[1,null,2]},{a:2, b:[null,2,3]})
     * we would get `{"a":2,"b":[null,2,3]}` but want we actaully want is `{"a":2,"b":[1,2,3]}`
     * therefor we need to roll our own explited copy of the extend function
     */
    var myExtend = jQuery.fn.extend = function() {
        var options, name, src, copy, copyIsArray, clone,
            target = arguments[ 0 ] || {},
            i = 1,
            length = arguments.length,
            deep = false;

        // Handle a deep copy situation
        if ( typeof target === "boolean" ) {
            deep = target;

            // Skip the boolean and the target
            target = arguments[ i ] || {};
            i++;
        }

        // Handle case when target is a string or something (possible in deep copy)
        if ( typeof target !== "object" && !$.isFunction( target ) ) {
            target = {};
        }

        // Extend jQuery itself if only one argument is passed
        if ( i === length ) {
            target = this;
            i--;
        }

        for ( ; i < length; i++ ) {

            // Only deal with non-null/undefined values
            if ( ( options = arguments[ i ] ) != null ) {

                // Extend the base object
                for ( name in options ) {
                    src = target[ name ];
                    copy = options[ name ];

                    // Prevent never-ending loop
                    if ( target === copy ) {
                        continue;
                    }

                    // Recurse if we're merging plain objects or arrays
                    if ( deep && copy && ( $.isPlainObject( copy ) ||
                        ( copyIsArray = Array.isArray( copy ) ) ) ) {

                        if ( copyIsArray ) {
                            copyIsArray = false;
                            clone = src && Array.isArray( src ) ? src : [];

                        } else {
                            clone = src && $.isPlainObject( src ) ? src : {};
                        }

                        // Never move original objects, clone them
                        if (Array.isArray(clone)) {
                            // !!! this is our hack !!!
                            var arr = target[ name ] = []
                            for (ai=0; ai<Math.max(clone.length, copy.length); ai++) {
                                if ( $.isPlainObject( clone[ai] ) ||  Array.isArray( clone[ai] ) ) {
                                    arr[ai] = myExtend( deep, clone[ai], copy[ai] );
                                } else {
                                    // console.log("copy", copy[ai], clone[ai])
                                    arr[ai] = ( copy[ai] != undefined && copy[ai] != null ) ? copy[ai] : clone[ai]
                                }
                            }
                        } else {
                            target[ name ] = myExtend( deep, clone, copy );
                        }

                    // Don't bring in undefined values
                    } else if ( copy !== undefined ) {
                        target[ name ] = copy;
                    }
                }
            }
        }

        // Return the modified object
        return target;
    };

    /**
     *
     */
    function hover(elements) {
        var lastEvent = {chartId: "", datasetIdx: -1, dataIdx: -1}
        var validElements = elements
            .filter(function(e){return e._model.hitRadius == undefined || e._model.hitRadius > 1})
            .forEach(function(e) {
                window.xx = e
                var event = {chartId: e._chart.canvas.attributes.id.nodeValue, datasetIdx: e._datasetIndex, dataIdx: e._index}
                if (!(event.chartId == lastEvent.chartId && event.datasetIdx == lastEvent.datasetIdx && event.dataIdx == lastEvent.dataIdx)) {
                    var dataSet = e._chart.config.data.datasets[event.datasetIdx]
                    // FIXME name for destinations should be availabe from css
                    var targetChartId = "weights"
                    var targetDataSetId = "weights"
                    if (dataSet.ext) {
                        parent.postMessage({t: "updateSeries", c: targetChartId, s: targetDataSetId, d: dataSet.ext }, "*")
                    }

                    lastEvent = event
                }
            })

    }

    /**
     * Computes the object from the custom attribute
     * attribute-string -> cleanings -> JSON#parse
     */
    function computeStyles(rules) {
        // console.log(rules)

        var filterLabels = function(chart) {
            labels = Chart.defaults.global.legend.labels.generateLabels(chart).filter(function (l){ return l.text});
            return labels;
        }

        var chartData = {
            data: { datasets: []},
            options: {
                legend: { labels: { generateLabels: filterLabels }},
                hover: { onHover: hover }
            }
        }

        for (var i=0; i<rules.length; i++) {
            var rule = rules[i].properties
            //console.log("props", rule)

            // skip empty properties
            if (!rule) continue

            // rule.properties contains content or -content
            var content = rule.content ? rule.content.value : rule["-content"] ? rule["-content"].value : "{}"
            // console.log("content", content)
            content = content.replace(/^["']+|["']+$/g, '').replace(/\\"/g,'"');
            // console.log("content", content)
            content = JSON.parse(content)
            // console.log("content", content)

            // merge content
            myExtend(true, chartData, content)
        }

        return chartData
    }


    /**
     * This is the actual jQuery plugin
     */
    $(document).ready(function() {
        CSSUtilities.define('async', true)

        //asynchronously call data method with callback
        $('canvas.chartjs').each(function(i, e) {
            CSSUtilities.getCSSRules(e, function(rules) {
                var element = $(e)
                chartData = computeStyles(rules)

                // log the chart data object
                console.log(element.attr("id"), JSON.stringify(chartData))

                // initialize missing chart data and add custom properties needed by the message handler
                chartData.data.datasets.forEach(function(ds){if (!ds.data) ds.data = []})
                chartData.data.dsIndex = {}

                // console.log("chartData", element.attr('id'), chartData)
                var chart = new Chart(element, chartData)

                // TODO instead of exposing the chart we expose an API
                element.data("chart", chart)
            })
        })
    })
}( jQuery ));

