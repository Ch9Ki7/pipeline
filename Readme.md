# Pipelines Framework
The Idea is to provide a framework on top of [Akka Graphs](http://doc.akka.io/docs/akka/2.4/scala/stream/stream-graphs.html). 
The Goal is to provide an easy way of prototyping data flows i.e. for trading strategies or just 
for plotting new indicators.

By using Akka Graphs a data flow like this:

![Simple Graph Example](./simple-graph-example1.png "Simple Graph Example")


looks almost exactly the same in the code

```scala
val g = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
  import GraphDSL.Implicits._
  val in = Source(1 to 10)
  val out = Sink.ignore
 
  val bcast = builder.add(Broadcast[Int](2))
  val merge = builder.add(Merge[Int](2))
 
  val f1, f2, f3, f4 = Flow[Int].map(_ + 10)
 
  // assembling the actual graph like in the picture above
  in ~> f1 ~> bcast ~> f2 ~> merge ~> f3 ~> out
              bcast ~> f4 ~> merge
              
  ClosedShape
})
```

## Example Use Case of _Pipelines_
To [illustrate](http://asciiflow.com/) which kind of data flows one should be able to generate with this 
tool consider the use case of a Portfolio Optimizer:

```


                                              +-----------+       +-----------+       +------------+      +----------------------+
                                              |retrn2perf +------>+MACD(x,y,z)+------>+collect all +----->+                      |
                                              +-----------+       +-----------+       |to Vector   |      |                      |
                                                  ^                                   +------------+      | optimize portfolio   |
                                                  |                                                       | backtest & update    |
                                                  |                                                       | realtime on new      |
                                                  |                                                       | events               |
                       +-------------+        +---------+         +----------+        +-------------+     |                      |
  Sources 1 to n  +--> |price2return +------->+bcast (3)+-------->+de mean(x)+------->+collect all  +---->+                      |
  (prices)             +-------------+        +---------+         +----------+        |to COV matrix|     |                      |
                                                  |                                   +-------------+     |                      |
                                                  |                                                       | use macd histrogram  |
                                                  |                                                       | as expected returns  |
                                                  ^                                                       |                      |
                                              +---------------------+                                     | rebalance whenever   |
                                              |collect all to Vector+------------------------------------>+ one position is      |
                                              +---------------------+                                     | stopped out (x sdev) |
                                                                                                          |                      |
                                                                                                          |                      |
                                                                                                          |                      |
                                                                                                          |                      |
                                                                                                          |                      |
                       +-------------+                                                                    |                      |
  Source Benchmark +-->+price2return +------------------------------------------------------------------->+                      |
  (prices)             +-------------+                                                                    |                      |
                                                                                                          |                      |
                                                                                                          |                      |
                                                                                                          |                      |
                                                                                                          +----------------------+
                                                                                                            |         |        |
                                                                                                            |         |        |
                                                                                                            |         |        |
                                                                                                            | alpha   | risk   | return
                                                                                                            |         |        |
                                                                                                            |         |        |
                                                                                                            |         |        |
                                                                                                            v         v        v
                                                                                                           plot      plot    plot
                                                                                                           sink      sink    sink


```

## Links
* http://doc.akka.io/docs/akka/2.4.12/scala/stream/index.html
* http://doc.akka.io/docs/akka/2.4.2/scala/stream/stream-graphs.html
* http://doc.akka.io/docs/akka/2.4.12/scala/stream/stream-customize.html 
* http://doc.akka.io/docs/akka/2.4.12/scala/stream/stream-composition.html

## Needed work
To achieve our goal we need to work on the following topics:

- [X] Synchronize Gaps over multiple sources/inlets
- [ ] Extend the Graphs DSL
- [ ] Make the source look like scala code -> [http://docs.scala-lang.org/style/indentation](http://docs.scala-lang.org/style/indentation)
- [ ] Re-stucture the code to be logically 
- [ ] Manage state and restart persistence
- [ ] Enable some basic plotting with probably multiple panels
- [ ] Implement a standard set of Quant Flows like 
  * [Indicators (EMA, MACD, A/D, ...)](http://ta-lib.org/function.html)
  * Satistics like covariances, std deviations, ...
  * Linear Regression, BayesianRegression, ... 
  * AI like Hidden Markov Models, Boltzman Machines, ... 

## Extending the Graphs DSL

This is how it looks like with the akka graph dsl:
```scala
      val g = RunnableGraph.fromGraph(GraphDSL.create(sink[OUT]()) { implicit b => sink =>
        import GraphDSL.Implicits._

        val cov = b.add(qt.fanIntoCovariance(rawSources.length, 6))
        val obsRet = b.add(qt.fanIntoRowVector(rawSources.length))
        val expRet = b.add(qt.fanIntoRowVector(rawSources.length))
        val bench = b.add(Source(tsSp5))
        val benchToRet = b.add(qt.price2Return()())
        val opt = b.add(qt.optimizePortfolio(rawSources.length, 50))
        val zip = b.add(ZipWith[KAD, E, E, OUT]((w, ooRisk, ooRet) => (w, ooRisk, ooRet)))

        rawSources.indices.map(i => {
          val src = b.add(Source(rawSources(i)))
          val toRet = b.add(qt.price2Return()())
          val bcRet = b.add(Broadcast[E](3))
          val toPerf = b.add(qt.return2Performance()())
          val macd = b.add(qt.macd(3, 5, 0, qt.sma)())


          src ~> toRet ~> bcRet.in
                          bcRet.out(0) ~> cov.in(i)
                          bcRet.out(1) ~> obsRet.in(i)
                          bcRet.out(2) ~> toPerf ~> macd.in
                                                    macd.outlets(0) ~> expRet.in(i)
                                                    macd.outlets(1) ~> Sink.ignore
                                                    macd.outlets(2) ~> Sink.ignore
        })

        expRet.out               ~> opt.in0ExpectedReturns
        obsRet.out               ~> opt.in1ObservedReturns
        cov.out                  ~> opt.in2Covariances
        bench      ~> benchToRet ~> opt.in3BenchmarkReturns
                                    opt.out0Weights           ~> zip.in0
                                    opt.out1Rebalanced        ~> Sink.ignore
                                    opt.out2PortfolioRisk     ~> Sink.ignore
                                    opt.out3PortfolioReturn   ~> Sink.ignore
                                    opt.out4BenchmarkReturn   ~> Sink.ignore
                                    opt.out5Alpha             ~> Sink.ignore
                                    opt.out6OneOverNRisk      ~> zip.in1
                                    opt.out7OneOverNReturn    ~> zip.in2
                                                                 zip.out ~> sink

        ClosedShape
      })
```

This is clearly far to verbose we would like to have something close to this
```scala
shelf     ~> price2Return ~> macd(12, 26, 9).histogram ~> fanIntoColumnVector    ~> optimizer.in0ExpectedReturns                    // shelf is a sequence of sources* and ignore unused macd outlets
                                  ~>                                                optimizer.in1ObservedReturns                    // automatically boradcast returns
                                  ~>                      fanIntoCovariance(100) ~> optimizer.in2Covariances                        // automatically boradcast returns
benchmark ~> price2Return ~>                                                        optimizer.in3BenchmarkReturns                   // operate an single sources as well
                                                                                    optimizer.out2PortfolioReturn  ~> plot(line)    // ignore the other two outlets by default

```

So the Graph DSL requires to be extend to support the following stuff:
 
* a "sequece of sources" connection to a "function providing a flow" `Seq[Source[_]] ~> () -> Flow[_]`
* a "function providing a flow" connection to a "function providing a flow" `() -> Flow[_] ~> () -> Flow[_]`
* a "function providing a flow" connection to a "function providing a sink" `() -> Flow[_] ~> () -> Sink[_]`
* a tpul of `(Inlet[_], outlet[_])` sould also be a possible DSL source and target like ` ~> (macd.in, macd.histogram) ~> ` 

There are also some nice to haves like:
* by default connect any outlet to an "ingore sink" so the user is not forced to use all ever provided 
outputs i.e. if someone does not want to plot the alpha output from the optimizer flow.  
* automatically put a `Broadcast` flow between elements as soon as an outlet is wired more then once 
like if ` x ~> y; x ~> z ` then automatically generate `x ~> bcast(2); bcast.out(0) ~> y; bcast.out(1) ~> z `

## Synchronizing Gaps
The simplest illustarion of the _Gap Problem_ is when you want to plot several lets say stock prices 
on the same chart. While you define a date range as well as a data flow for all of them it is not 
guarantied that all stock symbols start at the defined date range (i.e. on stock might just be emitted 
one year ago while requesting 1.1.2000 - now)

Another Gap Problem is when one stock has a missing price while another one has not (i.e. offday in US 
but not in EU). While we have just such single days one might not even notice that from a particular 
time on the whole data is shifted by one day and therefore completely wrong. A similiar problem occurs 
if you mix different timeframes like daily and weekly data. You not only want to allign the data but
also fill in the gaps i.e. by just using the last one, a linera interpolation or even a brownian motion
 
And last but not least there are gaps due to the logic in the flow - consider following graph:

```scala
prices ~> bcast ~> sma(20) ~> mergeCrossOver ~> plot
          bcast ~>         ~> mergeCrossOver 
```

In this case you would end up in the `mergeCrossOver` with the price and a moving average which is 20 
days ahead of the prices. Well a quick and dirty fix for this particular problem might be the following 
graph:

 ```scala
 prices ~> bcast ~> sma(20)  ~> mergeCrossOver ~> plot
           bcast ~> skip(20) ~> mergeCrossOver 
 ```

But we are aiming for a more general solution which fixes all the cases where the data stream needs to 
be aligned and where gaps need to be filled.

## Manage State and Restart Persistence
The Idea of the _Source_ is that it is not only able to download historical stock prices but also wait
for ne prices to arrive. So you process the pipeline based on historical data and then wait until new
data arrives and update the existing portfolio (chart).

The question now is how do we handle the potential case that one might shutdown/restart the machine.
Should we just dowload and start from the beginning? While this might work fine for smaller data sets
but what if one makes a pipeline using 20 years of data and 1000 assets? the backtest process might
take a long while. Or even worse what if one has trained a neural net? 
 
While this might not he be most needed feature at the beginning it might still be worth thinking about 
it and maybe one comes up with a good idea.
 
## Plotting

tbd

## Standard Set of Quant Flows
Once the DSL and the example use case is in place it makes sence to implement a standard set of flows
i.e. trading indicators like [Indicators (EMA, MACD, A/D, ...)](http://ta-lib.org/function.html) and 
other statistical measures like covariances (exponetially weighted covariacne) and standard deviation 
and such. Also providing some machine learning "Block" meight be of interest.


## Building
just execute `gradlew build`

## Contributing
The recommended setup is intellij >= 2016.3 using this import settings:

![import settings](./intellij_import_settings.png "import settings")

To get started the main class is `gui/kic.graph.ui.HomeViewController` which is a java class. More 
precisely its a java FX controller class which executes the _pipelines_ with a click on a button. If
you feel lucky and want to mess with the UI install the [Scene Builder](http://gluonhq.com/labs/scene-builder/) 
to edit the fxml files.

The graph processing starts in `pipeline/kic.pipeline.Data` which is the starting point in the scala
world. This is also the place where we download quotes from finance.yahoo.com. All the quant flows are 
in the `QuantFlows` class. 
 
I have not decided for a particular loggin framework yet so I have introduced and intermediate trait
which is currently just doing _println_. The idea is to just implement a proper Logger later on in 
this particular trait.

Custom stages like the synchronization thing and the portfolio optimizer are in the _stages_ package.

Everything under scratch is just for intermediary trial and error practice in whatever - i.e. how the
implicits work, or how do generics work and stuff. This package will obviously entirely disappear in the 
future as the scala skills get better.

Note: You can clearly structure it better this is just the starting point.